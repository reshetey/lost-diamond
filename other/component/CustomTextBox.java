package com.ximad.lostdiamond.component;


import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.util.MathUtilities;

public class CustomTextBox extends VerticalFieldManager {

	private int managerWidth;
	private int managerHeight;

	private int curPosition = 0;
	private int maxPosition = 0;
	
	private LabelField labelField;
	
	int fontColor = /*Consts.FONT_GRAY_COLOR*/Color.WHITE;

	public CustomTextBox(int width, int height, Font font, long additionalStyle) {
		super(VerticalFieldManager.VERTICAL_SCROLL
			| VerticalFieldManager.VERTICAL_SCROLLBAR
			| VerticalFieldManager.FOCUSABLE);
		managerWidth = width;
		managerHeight = height;

		labelField = new LabelField("", LabelField.NON_FOCUSABLE | additionalStyle) {
			public void paint(Graphics g) {
				g.setColor(fontColor);
				super.paint(g);
			}
		};
		labelField.setFont(font);

		add(labelField);
	}
	
	public void paint(Graphics g){
        g.setColor(fontColor);
        super.paint(g);
	}
	
	public void setFontColor(int _fontColor) {
		fontColor = _fontColor;
	}

	public void sublayout(int w, int h) {
		
		if (managerWidth == 0) {
			managerWidth = w;
		}
		if (managerHeight == 0) {
			managerHeight = h;
		}

		layoutChild(labelField, managerWidth, managerHeight);
		setPositionChild(labelField, 0, 0);

		super.sublayout(managerWidth, managerHeight);
		setExtent(managerWidth, managerHeight);
	}

	public String getText() {
		return labelField.getText();
	}

	public void setText(String text) {
		labelField.setText(text);
	}
	
	private void findMaxScrollPos() {
		maxPosition = getVirtualHeight() - getVisibleHeight();
		
		if (maxPosition < 0) {
			maxPosition = 0;
		}
	}
	
	public void setCurScrollPos(int value) {
		 curPosition = value;
	}

	public void scrollText(int offset) {
		if (curPosition == 0) {
			findMaxScrollPos();
		}
		
		if (((curPosition >= maxPosition) && offset > 0)) {
			return;
		}
		
		curPosition += offset;
		curPosition = MathUtilities.clamp(0, curPosition, maxPosition);
		
		this.setVerticalScroll(curPosition);
		invalidate();
	}
}
package com.ximad.lostdiamond.ad;

//import org.json.me.JSONArray;
//import org.json.me.JSONException;
//import org.json.me.JSONObject;
import com.transpera.sdk.org.json.me.JSONArray;
import com.transpera.sdk.org.json.me.JSONException;
import com.transpera.sdk.org.json.me.JSONObject;
import com.ximad.lostdiamond.DataManager;
import com.ximad.lostdiamond.Utils;
import com.ximad.lostdiamond.ad.script.BannerBitmapField;
import com.ximad.lostdiamond.component.CustomTextBox;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import java.io.DataInputStream;

public final class AdHandler extends Thread {
	private static String requestData;
	private static String tempClickLink;
	private static String clickLink;
	private static String imageLink;
	private static String bannerText;

	//AD config
	private static int curFrequency;
	private static int curPopupPeriod;
	private static boolean transperaEnable;
	//Short name
	private static String short_name;
	//Transpera CCH, PID
	private static String transpera_CCH;
	private static int transpera_PID;
	
	private static long lastPopupClickTime;

	private static String connectionSuffix;

	private static Bitmap defaultBanner;

	private static HttpConnection mainConnection;

	private static BannerBitmapField bannerField;
	private static CustomTextBox bannerTextBox;

	public AdHandler() {
		requestData = "";
		tempClickLink = com.ximad.lostdiamond.ad.Consts_Ad.DEFAULT_CLICK_LINK;
		clickLink = Consts_Ad.DEFAULT_CLICK_LINK;
		imageLink = "";
		bannerText = "";

		//AD config
		curFrequency = Consts_Ad.DEFAULT_FREQUENCY;
		curPopupPeriod = Consts_Ad.DEFAULT_POPUP_PERIOD;
		transperaEnable = Consts_Ad.DEFAULT_TRANSPERA_ENB;
		//Short name
		short_name = Consts_Ad.DEFAULT_SHORT_NAME;
		
		
		connectionSuffix = "";

		defaultBanner = Bitmap
				.getBitmapResource(Consts_Ad.DEFAULT_BANNER_IMAGE);

		//
		// BANNER FIELD
		//
		bannerField = new BannerBitmapField(defaultBanner,
				BitmapField.NON_FOCUSABLE) {
			public void paint(Graphics g) {
				if (!bannerTextBox.getText().equals("")) {
					this.setBitmap(null);
				}

				super.paint(g);
			}
		};

		//
		// BANNER TEXT BOX
		//
		bannerTextBox = new CustomTextBox(Consts_Ad.BANNER_DEFAULT_WIDTH,
				Consts_Ad.BANNER_DEFAULT_HEIGHT, null,
                /*.getUtils.getFont(Consts_Ad.FONT_FAMILY_NAME,
						Font.BOLD, Consts_Ad.FONT_AD_TEXT_SIZE)*/
				DrawStyle.HCENTER | DrawStyle.VCENTER);
		bannerTextBox.setFontColor(Color.WHITE);
		bannerTextBox.setVerticalScroll(0);
		
		init_ad_config();
	}

	private void init_ad_config()
	{
		try 
		{
			if(!loadParameters())
			{
				//Utils.showMessage("none!");
			}			
			else
			{
				//Utils.showMessage("sucsess");
			}
		} 
		catch (JSONException e) 
		{
			//Utils.showMessage("error: " + e);
			UiApplication.getUiApplication().invokeLater(new Runnable() 
			{
				public void run() 
				{
					init_ad_config();
				}
			}, 30000, false);			
		}
	}
	
	public void run() {

		lastPopupClickTime = 0;

		while (true) {
			try {
				//������ ���������� ������� ������� - ��������� �� �����
				if (!DataManager.isAdPopupNeeded) {
					if (System.currentTimeMillis() - lastPopupClickTime > curPopupPeriod) {
						DataManager.isAdPopupNeeded = true;
					}
				}

				// ������� �� ����������� � ����
				if (!UiApplication.getApplication().isForeground()) {

					showDefaultBanner();

					// SLEEP
					try {
						Thread.sleep(getCurFrequency());
					} catch (InterruptedException e1) {
					}

					continue;
				}

				// check internet connection
				if (CoverageInfo.isOutOfCoverage()) {
					showDefaultBanner();

					// SLEEP
					try {
						Thread.sleep(getCurFrequency());
					} catch (InterruptedException e1) {
					}
					continue;
				}

				// IF "NO ADS"
				if (!AdHandler.loadRequestData()) {

					if (mainConnection != null) {
						mainConnection.close();
					}

					showDefaultBanner();

					// SLEEP
					try {
						Thread.sleep(getCurFrequency());
					} catch (InterruptedException e1) {
					}
					continue;
				}

				if (mainConnection != null) {
					mainConnection.close();
				}

				Bitmap newBannerImage = null;

				// AD WITH IMAGE
				if (!imageLink.equals("")) {

					try {
						newBannerImage = AdHandler.getBannerImage();
					} catch (Exception e) {
						// Utils.showMessage("encoded banner image creating error! - "
						// + e.getMessage());
					}

					if (newBannerImage == null) {
						showDefaultBanner();

						// SLEEP
						try {
							Thread.sleep(getCurFrequency());
						} catch (InterruptedException e1) {
						}
						continue;

						// if loading image error (returned default banner),
						// then display one of default ad texts
					} else if (imageLink.equals(Consts_Ad.ERROR_IMAGE_LINK)) {
						// imageLink = "";
						bannerText = Utils.getRandomDefaultAdText();

						synchronized (Application.getEventLock()) {
							bannerTextBox.setText(bannerText);
						}

						// new normal banner with image
					} else {
						synchronized (Application.getEventLock()) {
							bannerField.setBitmap(newBannerImage);
						}

					}

					// AD WITH TEXT
				} else {
					synchronized (Application.getEventLock()) {
						bannerTextBox.setText(bannerText);
					}
				}

				// ������ �� ������� �� ������ ��������������� ������
				// ����� ������ ��������� ��������
				clickLink = tempClickLink;

			} catch (Exception e) {

				try {
					if (mainConnection != null) {
						mainConnection.close();
					}
				} catch (Exception ex) {
				}

				showDefaultBanner();

				// SLEEP
				try {
					Thread.sleep(getCurFrequency());
				} catch (InterruptedException e1) {
				}
				continue;
			}

			// SLEEP
			try {
				Thread.sleep(getCurFrequency());
			} catch (InterruptedException e1) {
			}
			continue;
		}
	}
	
	public static boolean loadParameters() throws JSONException
	{
		String params = "";
		
		params = loadData(Consts_Ad.SCRIPT_PARAMS_URL);

        if(params.length() == 0)
		{
			return false;
		}
		
			JSONObject json = new JSONObject(params);
			
			//get config
			JSONObject extra = json.getJSONObject("extra");
			
			try 
			{    
				curFrequency = extra.getInt("refresh_rate");
	    		curFrequency = curFrequency * 1000;     		
			}
	    	catch (JSONException e) 
	    	{
	    		curFrequency = Consts_Ad.DEFAULT_FREQUENCY;
	    		//Utils.showMessage("Exception in parsing curFrequency. " + e);
	    	}
	    	
	    	try 
			{    
	    		curPopupPeriod = extra.getInt("ads_free_rate");

                curPopupPeriod = curPopupPeriod * 60 * 1000;

                if (curPopupPeriod > Consts_Ad.DEFAULT_POPUP_PERIOD){
                    curPopupPeriod = Consts_Ad.DEFAULT_POPUP_PERIOD;
                }
            }
	    	catch (JSONException e) 
	    	{
	    		curPopupPeriod = Consts_Ad.DEFAULT_POPUP_PERIOD;
	    		//Utils.showMessage("Exception in parsing curPopupPeriod. " + e);
	    	}
	    	
	    	try 
			{    
				transperaEnable = extra.getBoolean("fullscreen_ad");
	    		
			}
	    	catch (JSONException e) 
	    	{
	    		transperaEnable = Consts_Ad.DEFAULT_TRANSPERA_ENB;;
	    		//Utils.showMessage("Exception in parsing transperaEnable. " + e);
	    	}
	    	
	    	
	    	//get Networks
			JSONArray networks_array = json.getJSONArray("networks");
			
			try 
			{    
				for(int i = 0; i < networks_array.length(); i++)
				{
					JSONObject jsonRation = networks_array.getJSONObject(i); 
					
					int net_id = jsonRation.getInt("id");
					
					if(net_id == 107)
					{
						//XIMAD SDK
						short_name = jsonRation.getString("param1");
					}	
				}
			}
	    	catch (JSONException e) 
	    	{
	    		short_name = Consts_Ad.DEFAULT_SHORT_NAME;
	    		//Utils.showMessage("Exception in parsing short name. " + e);
	    	}
	    	
	    	//get iNetworks
			JSONArray inetworks_array = json.getJSONArray("inetworks");
			
			try 
			{    
				for(int i = 0; i < inetworks_array.length(); i++)
				{
					JSONObject inetwork = inetworks_array.getJSONObject(i); 
						
					int net_id = inetwork.getInt("id");
						
					if(net_id == 111)
					{
						//TRANSPERA
						transpera_CCH = inetwork.getString("param1");
						transpera_PID = inetwork.getInt("param2");
					}
				}
			}
	    	catch (JSONException e) 
	    	{
	    		transpera_CCH = Consts_Ad.DEFAULT_TRANSPERA_CCH;
	    		transpera_PID = Consts_Ad.DEFAULT_TRANSPERA_PID;
	    		//Utils.showMessage("Exception in parsing short name. " + e);
	    	}
	    	
	    return true;
		
	}

	private static boolean loadRequestData() 
	{
			requestData = loadData(Consts_Ad.SCRIPT_AD_URL + "&name=" + short_name);
						
			if(requestData == "")
			{
				//Utils.showMessage("Ad load error");
			}
			
			requestData = Utils.replaceString(requestData, "&amp;", "&");

			// NO ADS
			if (requestData.indexOf(Consts_Ad.NO_ADS_STRING) != -1) 
			{
				return false;
			}
			else 
			{
				// get click link (destination link)
				tempClickLink = "";
				int beginLinkIndex = requestData
						.indexOf(Consts_Ad.A_HREF_STRING)
						+ Consts_Ad.A_HREF_STRING.length();
				int endLinkIndex = requestData.indexOf("\"", beginLinkIndex);
				tempClickLink = requestData.substring(beginLinkIndex,
						endLinkIndex);

				// get ad text
				bannerText = "";

				beginLinkIndex = requestData.indexOf("\">") + "\">".length();
				endLinkIndex = requestData.indexOf("</a>", beginLinkIndex);

				if ((beginLinkIndex != -1 + "\">".length())
						&& (endLinkIndex != -1)
						&& (beginLinkIndex != endLinkIndex)
						&& requestData.indexOf("\"></a>") == -1) {
					bannerText = requestData.substring(beginLinkIndex,
							endLinkIndex);

					// ���� ������ ������ ���� ������ �� �������� ��� <html>,
					// �� ��� �� ����� �������
					if (bannerText.indexOf(Consts_Ad.SRC_STRING) != -1
							|| bannerText.indexOf("<html>") != -1) {
						bannerText = "";
					}
				} else {
					bannerText = "";
				}

				// get image link
				imageLink = "";

				if (bannerText.equals("")) 
				{
					beginLinkIndex = requestData.indexOf(Consts_Ad.SRC_STRING)
							+ Consts_Ad.SRC_STRING.length();
					if (beginLinkIndex != -1 + Consts_Ad.SRC_STRING.length()) 
					{
						endLinkIndex = requestData
								.indexOf("\"", beginLinkIndex);

						imageLink = requestData.substring(beginLinkIndex,
								endLinkIndex);
					} 
					else 
					{
						imageLink = "";
					}
				} 
				else 
				{
					imageLink = "";
				}

				// if ad image and ad text is null - return false (no ads)
				if (bannerText.equals("") && imageLink.equals(""))
				{
					return false;
				}

				//curPopupPeriod = 2 * 60 * 1000;
			}

		return true;
	}
	
	private static String loadData(String url) 
	{
		DataInputStream in = null;
		String received_data = "";

		// GET CONNECTION SUFFIX
		connectionSuffix = Utils.getConnectionSuffix();

		try 
		{

			int loopCount = 0;
			String tempUrl = url;

			while (true) 
			{

				mainConnection = (HttpConnection) Connector.open(tempUrl
						+ connectionSuffix);
				mainConnection.setRequestProperty(Consts_Ad.USER_AGENT_STR,
						Utils.getUserAgent());
				mainConnection.setRequestMethod(HttpConnection.GET);
				mainConnection.setRequestProperty("Connection", "Keep-Alive");

				if (mainConnection == null) {
					return "";
				}

				// Get the status code, causing the connection to be made
				int status = mainConnection.getResponseCode();

				// REDIRECT
				if (status == HttpConnection.HTTP_TEMP_REDIRECT
						|| status == HttpConnection.HTTP_MOVED_TEMP
						|| status == HttpConnection.HTTP_MOVED_PERM) 
				{

					// ���� ����������� �������������� ��������� >= 5, ��
					// �������
					if (loopCount++ == 5) {
						mainConnection.close();

						return "";
					}

					// Get the new location and close the connection
					tempUrl = mainConnection.getHeaderField("Location");
					mainConnection.close();

					// NO REDIRECT - load image
				} 
				else 
				{
					break;
				}
			}

			int length = (int) mainConnection.getLength();

			byte[] data = null;

			// loading request data (byte[])
			if (length != -1) 
			{
				data = new byte[length];
				in = new DataInputStream(mainConnection.openInputStream());
				int len = (int) mainConnection.getLength();
				if (len > 0) 
				{
					int actual = 0;
					int bytesread = 0;

					while ((bytesread != len) && (actual != -1)) 
					{
						actual = in.read(data, bytesread, len - bytesread);
						bytesread += actual;
					}
				}
			}

			if (in != null)
			{
				in.close();
			}

			received_data = new String(data);
			
			

		} 
		catch (Exception e) 
		{

			try 
			{
				if (in != null) 
				{
					in.close();
				}
			} 
			catch (Exception ex)
			{
			}

			return "";
		} 
		finally
		{
			try 
			{
				if (in != null) 
				{
					in.close();
				}

			} 
			catch (Exception e)
			{
				return "";
			}
		}

		return received_data;
	}

	private static Bitmap getBannerImage() {

		HttpConnection connection = null;
		DataInputStream in = null;
		byte[] data = null;
		Bitmap bitmap = null;

		try {
			String tempUrl = imageLink;

			int loopCount = 0;

			while (true) {
				connection = (HttpConnection) Connector.open(tempUrl
						+ connectionSuffix);
				connection.setRequestProperty(Consts_Ad.USER_AGENT_STR, Utils
						.getUserAgent());
				connection.setRequestMethod(HttpConnection.GET);
				connection.setRequestProperty("Connection", "Keep-Alive");

				if (connection == null) {
					imageLink = Consts_Ad.ERROR_IMAGE_LINK;
					return defaultBanner;
				}

				// Get the status code, causing the connection to be made
				int status = connection.getResponseCode();

				// REDIRECT
				if (status == HttpConnection.HTTP_TEMP_REDIRECT
						|| status == HttpConnection.HTTP_MOVED_TEMP
						|| status == HttpConnection.HTTP_MOVED_PERM) {

					// ���� ����������� �������������� ��������� >= 5, ��
					// �������
					if (loopCount++ == 5) {
						connection.close();

						imageLink = Consts_Ad.ERROR_IMAGE_LINK;
						return defaultBanner;
					}

					// Get the new location and close the connection
					tempUrl = connection.getHeaderField("Location");
					connection.close();

					// NO REDIRECT - load image
				} else {
					break;
				}

			}

			int length = (int) connection.getLength();

			if (length != -1) {
				data = new byte[length];
				in = new DataInputStream(connection.openInputStream());
				int len = (int) connection.getLength();
				if (len > 0) {
					int actual = 0;
					int bytesread = 0;

					while ((bytesread != len) && (actual != -1)) {
						actual = in.read(data, bytesread, len - bytesread);
						bytesread += actual;
					}
				}
			}

			bitmap = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);

			if (in != null) {
				in.close();
			}

			if (connection != null) {
				connection.close();
			}

			// ���� ��� ADSPACER ��� ������ ������ 10x10, �� ���������� ���� ��
			// ����������� �������
			if (imageLink.indexOf("adspacer") != -1 || bitmap.getWidth() < 10
					|| bitmap.getHeight() < 10) {
				imageLink = Consts_Ad.ERROR_IMAGE_LINK;
				return defaultBanner;
			}

			try {
				return Utils.resizeBanner(data, Consts_Ad.BANNER_DEFAULT_WIDTH,
						Consts_Ad.BANNER_DEFAULT_HEIGHT);
			} catch (Exception e) {
				// Utils.showMessage("������ ���������������");
				return bitmap;
			}

		} catch (Exception e) {
			try {
				if (in != null) {
					in.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception ex) {
			}

			// ���� ������ ��� ��������, �� ���������� ���� �� �����������
			// �������
			imageLink = Consts_Ad.ERROR_IMAGE_LINK;
			return defaultBanner;

		} finally {
			try {
				if (in != null) {
					in.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception ex) {
			}
		}

	}

	// shows default "XIMAD" banner
	private void showDefaultBanner() {
		bannerText = "";

		clickLink = Consts_Ad.DEFAULT_CLICK_LINK;

		// ��� ���������� ��� ����, ����� ��������, ��� ��� �� ���������
		// �������. � ������ ��������� ������� imageLink ������ ���� ����� ""
		imageLink = Consts_Ad.DEFAULT_IMAGE_LINK;

		curFrequency = Consts_Ad.DEFAULT_FREQUENCY;

		synchronized (Application.getEventLock()) {
			bannerTextBox.setText("");
			bannerField.setBitmap(defaultBanner);
		}
	}

	// GETTERS
	private long getCurFrequency() {
		return curFrequency;
	}

	public static String getClickLink() {
		return clickLink;
	}

	public static BitmapField getBannerField() {
		return bannerField;
	}

	public static Bitmap getBannerFieldImage() {
		return bannerField.getBitmap();
	}

	public static CustomTextBox getBannerTextBox() {
		return bannerTextBox;
	}

	public static Bitmap getDefaultBanner() {
		return defaultBanner;
	}
	
	public static boolean getTransperaEnb() {
		return transperaEnable;
	}
	
	
	public static String getTransperaCCH() {
		return transpera_CCH;
	}
	public static int getTransperaPID() {
		return transpera_PID;
	}

	// SETTERS

	public static void setAdTextColor(int color) {
		bannerTextBox.setFontColor(color);
	}

	// ������������� ������� ����� ����� �� ������� �� ������
	public static void savePopupClickTime() {
		lastPopupClickTime = System.currentTimeMillis();
	}
}

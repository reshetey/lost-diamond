package com.ximad.lostdiamond;

public final class DataManager {

	private static StringBuffer curKeyword;

	private static int currentSoundLevel = 30;

	// FOR FREE VERSION
	public static boolean isAdPopupNeeded;
	public static boolean isVideoAdReady;
//	public static int shownJokesNumber;
	//

	public DataManager()
	{
		//init_app_settings
		setDefaultSettings();
	}

	private void setDefaultSettings() {
		curKeyword = new StringBuffer("");

		isAdPopupNeeded = true;
		isVideoAdReady = false;
	}

	public static void setCurKeyword(String _keyword) {
		curKeyword = new StringBuffer(_keyword);
	}

	public static String getCurKeyword() {
		return curKeyword.toString();
	}

	// sound level control
	public static int getCurrentSoundLvl() {
		return currentSoundLevel;
	}

	public static void setCurrentSoundLvl(int lvl) {
		currentSoundLevel = lvl;
	}
}
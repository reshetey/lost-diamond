package com.ximad.lostdiamond.device;


public class Dimension360480 extends ADimension {
    public int getTierExtentX() {
        return 190;
    }

    public int getTierExtentY() {
        return 240;
    }

    public int getTierPopupX() {
        return 95;
    }

    public int getTierPopupY() {
        return 110;
    }

    public int getTierSelectorW() {
        return 148;
    }

    public int getTierSelectorH() {
        return 53;
    }

    public int getTierSelectorX() {
        return 18;
    }

    public int getSettingsPopupX() {
        return 70;
    }

    public int getSettingsPopupY() {
        return 116;
    }

    public int getWidth() {
        return 360;
    }

    public int getHeight() {
        return 480;
    }

    public int getBlocksSelectorX() {
        return 100;
    }

    public int getBlocksSelectorY() {
        return 102;
    }

    public int getShiftX() {
        return 31;
    }

    public int getShiftY() {
        return 151;
    }

    public String getDirection() {
        return "v";
    }

    public int getLevelX(int w) {
        return 132;
    }

    public int getLevelY() {
        return 17;
    }

    public int getActiveTierX(int w) {
        return 160;
    }

    public int getActiveTierY() {
        return 101;
    }

    public int getPointX(int w) {
        return 270;
    }

    public int getPointY() {
        return 17;
    }

    public int getMoveX(int w) {
        return 130;
    }

    public int getMoveY() {
        return 60;
    }

    public int getMinX(int w) {
        return 265;
    }

    public int getMinY() {
        return 60;
    }

    public int getLevelImagesInRow() {
        return 4;
    }

    public int getLevelArrowWidth() {
        return 137;
    }

    public int getLevelArrowHeight() {
        return 32;
    }

    public int getLevelArrowX() {
        return (getWidth() - getLevelArrowWidth())/2 - 4;
    }

    public int getLevelShiftY() {
        return 17;
    }

    public int getConfirmPositionX() {
        return 60;
    }

    public int getConfirmPositionY() {
        return 106;
    }

    public boolean isTouchble(){
        return true;
    }

    public int getBannerWidth() {
        return 300;
    }

    public int getBannerHeight(){
        return 50;
    }

    public int getBannerX(){
        return 30;
    }

    public int getBannerY(){
        return 360;
    }
}
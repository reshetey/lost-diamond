package com.ximad.lostdiamond.device;

public class Dimension320240 extends ADimension {
    public int getTierExtentX() {
        return 127;
    }

    public int getTierExtentY() {
        return 160;
    }

    public int getTierPopupX() {
        return 96;
    }

    public int getTierPopupY() {
        return 40;
    }

    public int getTierSelectorW() {
        return 92;
    }

    public int getTierSelectorH() {
        return 32;
    }

    public int getTierSelectorX() {
        return 18;
    }

    public int getTierSelectorY() {
        return 18;
    }

    public int getTierSelectorGap() {
        return 30;
    }

    public int getSettingsExtentX() {
        return 160;
    }

    public int getSettingsExtentY() {
        return 127;
    }

    public int getSettingsPopupX() {
        return 80;
    }

    public int getSettingsPopupY() {
        return 44;
    }

    public int getWidth() {
        return 320;
    }

    public int getHeight() {
        return 240;
    }

    public int getBlocksSelectorW() {
        return 103;
    }

    public int getBlocksSelectorH() {
        return 40;
    }

    public int getBlocksSelectorGap() {
        return 44;
    }

    public int getBlocksSelectorX() {
        return 116;
    }

    public int getBlocksSelectorY() {
        return 34;
    }

    public int getSide() {
        return 33;
    }

    public int getSide(int s) {
        if (s == 2){
            return 66;
        }

        if (s == 3){
            return 98;
        }

        return 32;
    }

    public int getShiftX() {
        return 102;
    }

    public int getShiftY() {
        return 22;
    }

    public String getDirection() {
        return "h";
    }

    public int getFontHeight() {
        return 16;
    }

    public int getLevelX(int w) {
        return 50 - w/2;
    }

    public int getLevelY() {
        return 30;
    }

    public int getActiveTierX(int w) {
        return 50 - w/2;
    }

    public int getActiveTierY() {
        return 210;
    }

    public int getPointX(int w) {
        return 50 - w/2;
    }

    public int getPointY() {
        return 75;
    }

    public int getMoveX(int w) {
        return 50 - w/2;
    }

    public int getMoveY() {
        return 120;
    }

    public int getMinX(int w) {
        return 50 - w/2;
    }

    public int getMinY() {
        return 165;
    }

    public int getLevelImageWidth() {
        return 50;
    }

    public int getLevelImageHeight() {
        return 50;
    }

    public int getLevelImageGapX() {
        return 8;
    }

    public int getLevelImageGapY() {
        return 8;
    }

    public int getLevelArrowWidth() {
        return 26;
    }

    public int getLevelArrowHeight() {
        return 26;
    }

    public int getLevelBestX(int w1) {
        return 40 - w1/2;
    }

    public int getLevelMinX(int w1) {
        return 32 - w1/2;
    }

    public int getLevelFont() {
        return 14;
    }

    public int getLevelMinFont() {
        return 12;
    }

    public int getConfirmPositionX() {
        return 80;
    }

    public int getConfirmPositionY() {
        return 36;
    }

    public int getConfirmExtentX() {
        return 160;
    }

    public int getConfirmExtentY() {
        return 127;
    }    

    public int getSettingsMarkH() {
        return 33;
    }

    public int getSettingsMarkW() {
        return 33;
    }

    public int getSettingsSoundMarkX() {
        return 22;
    }

    public int getSettingsSoundMarkY() {
        return 10;
    }

    public int getSettingsMusicMarkX() {
        return 22;
    }

    public int getSettingsMusicMarkY() {
        return 42;
    }

    public int getBannerWidth() {
        return 200;
    }

    public int getBannerHeight(){
        return 33;
    }

    public int getBannerX(){
        return 60;
    }

    public int getBannerY(){
        return 180;
    }
}

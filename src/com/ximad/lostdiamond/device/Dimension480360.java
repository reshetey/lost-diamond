package com.ximad.lostdiamond.device;

public class Dimension480360 extends ADimension {
    public int getTierExtentX() {
        return 190;
    }

    public int getTierExtentY() {
        return 240;
    }

    public int getTierPopupX() {
        return 145;
    }

    public int getTierPopupY() {
        return 60;
    }

    public int getTierSelectorW() {
        return 138;
    }

    public int getTierSelectorH() {
        return 53;
    }

    public int getTierSelectorX() {
        return 154;
    }

    public int getTierSelectorY() {
        return 120;
    }

    public int getSettingsPopupX() {
        return 120;
    }

    public int getSettingsPopupY() {
        return 70;
    }

    public int getHeight() {
        return 360;
    }

    public int getWidth() {
        return 480;
    }

    public int getBlocksSelectorX() {
        return 174;
    }

    public int getBlocksSelectorY() {
        return 54;
    }

    public int getShiftX() {
        return 151;
    }

    public int getShiftY() {
        return 31;
    }

    public String getDirection() {
        return "h";
    }

    public int getLevelX(int w) {
        return 75 - w/2;
    }

    public int getLevelY() {
        return 50;
    }

    public int getActiveTierX(int w) {
        return 75  - w/2;
    }

    public int getActiveTierY() {
        return 322;
    }

    public int getPointX(int w) {
        return 75  - w/2;
    }

    public int getPointY() {
        return 118;
    }

    public int getMoveX(int w) {
        return 75  - w/2;
    }

    public int getMoveY() {
        return 186;
    }

    public int getMinX(int w) {
        return 75  - w/2;
    }

    public int getMinY() {
        return 254;
    }

    public int getBannerWidth() {
        return 300;
    }

    public int getBannerHeight(){
        return 50;
    }

    public int getBannerX(){
        return 90;
    }

    public int getBannerY(){
        return 280;
    }
}

package com.ximad.lostdiamond.device;

public interface Dimension {
    int getTierExtentX();
    int getTierExtentY();
    int getTierPopupX();
    int getTierPopupY();
    int getTierSelectorW();
    int getTierSelectorH();
    int getTierSelectorX();
    int getTierSelectorY();
    int getTierSelectorGap();

    int getSettingsExtentX();
    int getSettingsExtentY();
    int getSettingsPopupX();
    int getSettingsPopupY();

    int getWidth();
    int getHeight();

    int getBlocksSelectorW();
    int getBlocksSelectorH();
    int getBlocksSelectorGap();
    int getBlocksSelectorX();
    int getBlocksSelectorY();

    int getSide();

    int getSide(int s);

    int getShiftX();
    int getShiftY();
    String getDirection();
    int getFontHeight();
    int getLevelX(int w);
    int getLevelY();
    //
    int getActiveTierX(int w);
    int getActiveTierY();
    int getPointX(int w);
    int getPointY();
    int getMoveX(int w);
    int getMoveY();
    int getMinX(int w);
    int getMinY();
    //level screen
    int getLevelImagesInRow();
    int getLevelImageWidth();
    int getLevelImageHeight();

    int getLevelImageGapX();

    int getLevelImageGapY();

    int getLevelArrowWidth();

    int getLevelArrowHeight();

    int getLevelArrowX();

    int getLevelShiftY();

    int getLevelBestX(int w1);

    int getLevelBestY();

    int getLevelMinX(int w1);

    int getLevelFont();

    int getLevelMinFont();

    int getConfirmExtentX();

    int getConfirmExtentY();

    int getConfirmPositionX();

    int getConfirmPositionY();

    int getSettingsMarkW();

    int getSettingsMarkH();

    int getSettingsSoundMarkX();

    int getSettingsSoundMarkY();

    int getSettingsMusicMarkX();

    int getSettingsMusicMarkY();

    boolean isTouchble();

    int getBannerWidth();

    int getBannerHeight();

    int getBannerX();

    int getBannerY();
}

package com.ximad.lostdiamond.device;

public class Dimension480320 extends ADimension {
    public int getTierExtentX() {
        return 190;
    }

    public int getTierExtentY() {
        return 240;
    }

    public int getTierPopupX() {
        return 145;
    }

    public int getTierPopupY() {
        return 40;
    }

    public int getTierSelectorW() {
        return 138;
    }

    public int getTierSelectorH() {
        return 53;
    }

    public int getTierSelectorX() {
        return 28;
    }

    public int getTierSelectorY() {
        return 27;
    }

    public int getSettingsPopupX() {
        return 120;
    }

    public int getSettingsPopupY() {
        return 40;
    }

    public int getWidth() {
        return 480;
    }

    public int getHeight() {
        return 320;
     }

    public int getBlocksSelectorX() {
        return 160;
    }

    public int getBlocksSelectorY() {
        return 28;
    }

    public int getBlocksSelectorGap() {
        return 65;
    }

    public int getShiftX() {
        return 170;
    }

    public int getShiftY() {
        return 11;
    }

    public String getDirection() {
        return "h";
    }
    
    public int getLevelX(int w) {
        return 80 - w/2;
    }

    public int getLevelY() {
        return 40;
    }

    public int getActiveTierX(int w) {
        return 80 - w/2;
    }

    public int getActiveTierY() {
        return 278;
    }

    public int getPointX(int w) {
        return 80 - w/2;
    }

    public int getPointY() {
        return 98;
    }

    public int getMoveX(int w) {
        return 80 - w/2;
    }

    public int getMoveY() {
        return 156;
    }

    public int getMinX(int w) {
        return 80 - w/2;
    }

    public int getMinY() {
        return 214;
    }

    public int getLevelImageHeight() {
        return 65;
    }

    public int getLevelMinX(){
        return 46;
    }

    public int getBannerWidth() {
        return 300;
    }

    public int getBannerHeight(){
        return 50;
    }

    public int getBannerX(){
        return 90;
    }

    public int getBannerY(){
        return 240;
    }

    public int getConfirmPositionY() {
        return 32;
    }
}

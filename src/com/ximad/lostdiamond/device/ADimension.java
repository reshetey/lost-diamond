package com.ximad.lostdiamond.device;


public abstract class ADimension implements Dimension {
    public int getTierSelectorY() {
        return 23;
    }

    public int getTierSelectorGap() {
        return 42;
    }

    public int getSettingsExtentX() {
        return 240;
    }

    public int getSettingsExtentY() {
        return 190;
    }

    public int getBlocksSelectorW() {
        return 154;
    }

    public int getBlocksSelectorH() {
        return 60;
    }

    public int getBlocksSelectorGap() {
        return 66;
    }

    public int getSide() {
        return 50;
    }

    public int getSide(int s) {
        return getSide();
    }

    public int getFontHeight() {
        return 24;
    }

    public int getLevelImagesInRow() {
        return 5;
    }

    public int getLevelImageWidth() {
        return 87;
    }

    public int getLevelImageHeight() {
        return 85;
    }

    public int getLevelImageGapX() {
        return 1;
    }

    public int getLevelImageGapY() {
        return 2;
    }

    public int getLevelArrowWidth() {
        return 20;
    }

    public int getLevelArrowHeight() {
        return 40;
    }

    public int getLevelArrowX() {
        return getWidth() - getLevelArrowWidth();
    }

    public int getLevelShiftY() {
        return 70;
    }

    public int getLevelBestX(int w1) {
        return 66 - w1/2;
    }

    public int getLevelBestY() {
        return 2;
    }

    public int getLevelMinX(int w) {
        return 16 - w/2;
    }

    public int getLevelFont() {
        return 18;
    }

    public int getLevelMinFont() {
        return 16;
    }

    public int getConfirmExtentX() {
        return 240;
    }

    public int getConfirmExtentY() {
        return 190;
    }

    public int getConfirmPositionX() {
        return 120;
    }

    public int getConfirmPositionY() {
        return 60;
    }

    public int getSettingsMarkH() {
        return 50;
    }

    public int getSettingsMarkW() {
        return 50;
    }

    public int getSettingsSoundMarkX() {
        return 30;
    }

    public int getSettingsSoundMarkY() {
        return 29;
    }

    public int getSettingsMusicMarkX() {
        return 30;
    }

    public int getSettingsMusicMarkY() {
        return 70;
    }

    public boolean isTouchble() {
        return false;
    }

}

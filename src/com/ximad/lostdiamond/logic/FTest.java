package com.ximad.lostdiamond.logic;

import com.ximad.lostdiamond.data.Tier;

import java.util.Vector;

public class FTest {
    private int size = 6;

    public static final int MAX_MOVE = 999;

    private int points[];

    private Level level;

    private int lev;

    private Tier tier;

    public FTest() {
        lev = 0;

        level = new Level();
    }

    /**
     * cleans level
     *
     * sets car & trucks for an actual level
     */
    private void init() {
        level.clean();
        level.setCar(tier.getCars()[lev]);
        
        for (int i = 0; i < tier.getTrucks()[lev].length; i++) {
            int[] truck = tier.getTrucks()[lev][i];

            level.setTruck(truck);
        }
    }

    /**
     * sets an actual tier
     * @param tier
     */
    public void setTier(Tier tier) {
        this.tier = tier;
    }

    /**
     *
     * @return true, if refresh needed
     */
    public int moveUp(){
        int x1 = level.getActiveX1();
        int y1 = level.getActiveY1();
        int x2 = level.getActiveX2();
        int y2 = level.getActiveY2();

        if ((x2 - x1) > 1){
            return -3;
        }

        if (y1 - 1 < 0){
            return -1;
        }


        y1--;
        y2--;

        boolean result = isOverlapped(x1, y1, x2, y2);

        if (!result){
            level.setActiveY1(y1);
            level.setActiveY2(y2);
        }

        return result ? -2 : 1;
    }

    /**
     * checks that active block has a one-step diagonal neighbour  
     * @param i
     * @return
     */
    private boolean isNear(int i){
        int x1 = level.getActiveX1();
        int x2 = level.getActiveX2();
        int y1 = level.getActiveY1();
        int y2 = level.getActiveY2();

        int x11 = level.getX1(i);
        int y11 = level.getY1(i);
        int x22 = level.getX2(i);
        int y22 = level.getY2(i);

        double xc1 = (x2 + x1) / 2.0;
        double yc1 = (y2 + y1) / 2.0;

        double xc2 = (x22 + x11)/2.0;
        double yc2 = (y22 + y11)/2.0;

        return ( (x2 - xc1) + (x22 - xc2) == Math.abs(xc2 - xc1) &&
                (y2 - yc1) + (y22 - yc2) == Math.abs(yc2 - yc1));
    }

    /**
     * move is forbidden
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    private boolean isOverlapped(int x1, int y1, int x2, int y2) {
        for (int i = 0; i < level.size(); i++) {
            if (level.isActive(i)){
                continue;
            }

            int x11 = level.getX1(i);
            int y11 = level.getY1(i);
            int x22 = level.getX2(i);
            int y22 = level.getY2(i);

            double xc1 = (x2 + x1) / 2.0;
            double yc1 = (y2 + y1) / 2.0;

            double xc2 = (x22 + x11)/2.0;
            double yc2 = (y22 + y11)/2.0;


            if ( (x2 - xc1) + (x22 - xc2) > Math.abs(xc2 - xc1) &&
                    (y2 - yc1) + (y22 - yc2) > Math.abs(yc2 - yc1)){
                return true;
            }
        }
        return false;
    }
    /**
       tries to move down
     */
    public int moveDown(){
        int x1 = level.getActiveX1();
        int y1 = level.getActiveY1();
        int x2 = level.getActiveX2();
        int y2 = level.getActiveY2();

        if (x2 - x1 > 1){
            return -3;
        }

        if (y2 + 1 > size){
            return -1;
        }

        y1++;
        y2++;

        boolean result = isOverlapped(x1, y1, x2, y2);

        if (!result){
            level.setActiveY1(y1);
            level.setActiveY2(y2);
        }

        return result ? -2 : 1;
    }
    /**
        if block is active, tries to move it left
        otherwise moves focus to the left
     */
    public int left(){
        if (level.isGo()){
            return moveLeft();
        }

        return nextLeft() ? 1 : 0;
    }
    /**
        if block is active, tries to move it right
        otherwise moves focus to the right
     */
    public int right(){
        if (level.isGo()){
            return moveRight();
        }

        return nextRight() ? 1 : 0;
    }
    /**
        if block is active, tries to move it up
        otherwise moves focus up
     */
    public int up(){
        if (level.isGo()){
            return moveUp();
        }

        return nextUp() ? 1 : 0;
    }
    /**
        if block is active, tries to move it up
        otherwise moves focus up
     */
    public int down(){
        if (level.isGo()){
            return moveDown();
        }

        return nextDown() ? 1 : 0;
    }

    /**
     * tries to move left
     * @return true if refresh needed
     */
    private int moveLeft(){
        int x1 = level.getActiveX1();
        int y1 = level.getActiveY1();
        int x2 = level.getActiveX2();
        int y2 = level.getActiveY2();

        if ((y2 - y1) > 1){
            return -3;
        }

        if (x1 - 1 < 0){
            return -1;
        }

        x1--;
        x2--;

        boolean result = isOverlapped(x1, y1, x2, y2);

        if (!result){
            level.setActiveX1(x1);
            level.setActiveX2(x2);
        }

        return result ? -2 : 1;
    }

    public boolean undo(){
        level.setGo(false);
        return level.undo();
    }

    /**
     * tries to move right
     * @return true if refresh needed
     */
    public int moveRight(){
        int x1 = level.getActiveX1();
        int y1 = level.getActiveY1();
        int x2 = level.getActiveX2();
        int y2 = level.getActiveY2();

        if (x2 == size && y2 == 3 && level.isCar()){
            level.setGo(false);
            int p = level.getMove();

            //rewrite points
            if (points[lev] == 0 || p < points[lev]){
                points[lev] = p;
            }

            if (points.length > lev + 1){
                loadLevel(lev + 1);
            } else {
                loadLevel(0);
            }

            return 1;
        }

        if ((y2 - y1) > 1){
            return -3;
        }

        if (x2 + 1 > size){
            return -1;
        }


        x1++;
        x2++;

        boolean result = isOverlapped(x1, y1, x2, y2);

        if (!result){
            level.setActiveX1(x1);
            level.setActiveX2(x2);
        }

        return result ? -2 : 1;
    }

//    private void increaseMove() {
//        level.increaseMove();
//        if (level.getMove() > MAX_MOVE){
//            loadLevel(lev);
//        }
//    }

    public Vector getFigures() {
        return level.list();
    }
    /**
        changes active/focusable state
     */
    public void changeGo(){
        level.changeGo();

        if (level.getMove() > MAX_MOVE){
            loadLevel(lev);
        }
    }

    public int getSize() {
        return size;
    }

    /**
     * active level
     * @return
     */
    public int getLevel() {
        return lev;
    }

    /**
     * users best result for the level
     * @return
     */
    public int getPoint() {
        return points[lev];
    }
    /**
     * number of moves user done for the level
     */
    public int getMove() {
        return level.getMove();
    }

    /**
     * minimum moves for the level
     * @return
     */
    public int getMin() {
        return tier.getMin()[lev];
    }

    /**
     * 
     * @return
     */
    public boolean nextLeft() {
        return next(new IRules() {
            public boolean checkBorder(int i) {
                return level.getActiveX1() >= level.getX2(i);
            }

            public boolean checkTube(int i) {
                int y1 = level.getActiveY1();
                int y2 = level.getActiveY2();

                int y11 = level.getY1(i);
                int y22 = level.getY2(i);

                double yc1 = (y2 + y1) / 2.0;

                double yc2 = (y22 + y11)/2.0;

                return (y2 - yc1) + (y22 - yc2) > Math.abs(yc2 - yc1);
            }

            public boolean checkRightHand(int i, int k) {
                return (level.getY1(i) + level.getY2(i))/2.0 < (level.getY1(k) + level.getY2(k))/2.0;
            }

            public boolean ckeckNear(int i) {
                return isNear(i);
            }
            public boolean checkNearTube(int i) {
                int x1 = level.getActiveX1();
                int x2 = level.getActiveX2();

                int x11 = level.getX1(i);
                int x22 = level.getX2(i);

                double xc1 = (x2 + x1) / 2.0;

                double xc2 = (x22 + x11)/2.0;

                return (x2 - xc1) + (x22 - xc2) == Math.abs(xc2 - xc1);
            }
        });
    }

    private boolean setActive(int k) {
        if (k > -1){
            level.setActive(k);
            return true;
        }
        return false;
    }

    public void loadLevel(int level) {
        lev = level;
        init();
    }

    public int[] getPoints() {
        return points;
    }

    public void setPoints(int[] points) {
        this.points = points;
    }

    private double getDistance(int i) {
        double dx = (level.getActiveX1() + level.getActiveX2())/2.0 - (level.getX1(i) + level.getX2(i))/2.0;
        double dy = (level.getActiveY1() + level.getActiveY2())/2.0 - (level.getY1(i) + level.getY2(i))/2.0;
        return Math.sqrt(dx*dx + dy*dy);
    }

    public boolean nextRight() {
        return next(new IRules() {
            public boolean checkBorder(int i) {
                return level.getActiveX2() <= level.getX1(i);
            }

            public boolean checkTube(int i) {
                int y1 = level.getActiveY1();
                int y2 = level.getActiveY2();

                int y11 = level.getY1(i);
                int y22 = level.getY2(i);

                double yc1 = (y2 + y1) / 2.0;

                double yc2 = (y22 + y11)/2.0;

                return (y2 - yc1) + (y22 - yc2) > Math.abs(yc2 - yc1);
            }

            public boolean checkRightHand(int i, int k) {
                return (level.getY1(i) + level.getY2(i))/2.0 > (level.getY1(k) + level.getY2(k))/2.0;
            }

            public boolean ckeckNear(int i) {
                return isNear(i);
            }

            public boolean checkNearTube(int i) {
                int x1 = level.getActiveX1();
                int x2 = level.getActiveX2();

                int x11 = level.getX1(i);
                int x22 = level.getX2(i);

                double xc1 = (x2 + x1) / 2.0;

                double xc2 = (x22 + x11)/2.0;

                return (x2 - xc1) + (x22 - xc2) == Math.abs(xc2 - xc1);
            }
        });

    }

    public boolean nextUp() {
        return next(new IRules() {
            public boolean checkBorder(int i) {
                return level.getActiveY1() >= level.getY2(i);
            }

            public boolean checkTube(int i) {
                int x1 = level.getActiveX1();
                int x2 = level.getActiveX2();

                int x11 = level.getX1(i);
                int x22 = level.getX2(i);

                double xc1 = (x2 + x1) / 2.0;

                double xc2 = (x22 + x11)/2.0;

                return (x2 - xc1) + (x22 - xc2) > Math.abs(xc2 - xc1);
            }

            public boolean checkRightHand(int i, int k) {
                return (level.getX1(i) + level.getX2(i))/2.0 > (level.getX1(k) + level.getX2(k))/2.0;
            }

            public boolean ckeckNear(int i) {
                return isNear(i);
            }

            public boolean checkNearTube(int i) {
                int y1 = level.getActiveY1();
                int y2 = level.getActiveY2();

                int y11 = level.getY1(i);
                int y22 = level.getY2(i);

                double yc1 = (y2 + y1) / 2.0;

                double yc2 = (y22 + y11) / 2.0;

                return (y2 - yc1) + (y22 - yc2) == Math.abs(yc2 - yc1);
            }
        });
    }

    public boolean nextDown(){
        return next(new IRules() {
            public boolean checkBorder(int i) {
                return level.getActiveY2() <= level.getY1(i);
            }

            public boolean checkTube(int i) {
                int x1 = level.getActiveX1();
                int x2 = level.getActiveX2();

                int x11 = level.getX1(i);
                int x22 = level.getX2(i);

                double xc1 = (x2 + x1) / 2.0;

                double xc2 = (x22 + x11)/2.0;

                return (x2 - xc1) + (x22 - xc2) > Math.abs(xc2 - xc1);
            }

            public boolean checkRightHand(int i, int k) {
                return (level.getX1(i) + level.getX2(i))/2.0 < (level.getX1(k) + level.getX2(k))/2.0;
            }

            public boolean checkNearTube(int i) {
                int y1 = level.getActiveY1();
                int y2 = level.getActiveY2();

                int y11 = level.getY1(i);
                int y22 = level.getY2(i);

                double yc1 = (y2 + y1) / 2.0;

                double yc2 = (y22 + y11) / 2.0;

                return (y2 - yc1) + (y22 - yc2) == Math.abs(yc2 - yc1);
            }

            public boolean ckeckNear(int i) {
                return isNear(i);
            }
        });
    }

    private boolean next(IRules rule){
        int  k = -1;
        int k1 = -1;
        int k2 = -1;
        int k3 = -1;
        int k4 = -1;


        double min1 = size * size;
        double min2 = size * size;
        double min3 = size * size;
        double min4 = size * size;

        for (int i = 0; i < level.size(); i++) {

            if (level.isActive(i)){
                continue;
            }

            if (rule.checkBorder(i)){
                double d = getDistance(i);
                //look to the tube first
                if (rule.checkTube(i)){
                    if (rule.checkNearTube(i)) {
                        if (d == min1){
                            if (rule.checkRightHand(i, k1)){
                                k1 = i;
                            }
                        }


                        if (d < min1){
                            k1 = i;
                            min1 = d;
                        }
                    }else {
                        if (d == min3){
                            if (rule.checkRightHand(i, k3)){
                                k3 = i;
                            }
                        }


                        if (d < min3){
                            k3 = i;
                            min3 = d;
                        }
                    }

                } else {
                    //right hand rule
                    if (rule.ckeckNear(i)) {
                        if (d == min2){
                            if (rule.checkRightHand(i, k2)){
                                k2 = i;
                            }
                        }

                        if (d < min2){
                            k2 = i;
                            min2 = d;
                        }
                    }else {
                        if (d == min4){
                            if (rule.checkRightHand(i, k4)){
                                k4 = i;
                            }
                        }

                        if (d < min4){
                            k4 = i;
                            min4 = d;
                        }
                    }
                }
            }
        }
        if (k1 != -1){
            return setActive(k1);
        }
        if (k2 != -1){
            return setActive(k2);
        }
        if (k3 != -1){
            return setActive(k3);
        }
        if (k4 != -1){
            return setActive(k4);
        }
        return setActive(k);
    }

    public int getActive() {
        return level.getActive();
    }

    public boolean setActive(float lastX, float lastY) {
        for (int i = 0; i < level.size(); i++){
            int x11 = level.getX1(i);
            int y11 = level.getY1(i);
            int x22 = level.getX2(i);
            int y22 = level.getY2(i);

            if (lastX > x11 && x22 > lastX && lastY > y11 && lastY < y22){
                level.setActive(i);
                level.setGo(true);
                return true;
            }
        }

        return false;
    }

    public void setGo(boolean b) {
        level.setGo(false);

        if (level.getMove() > MAX_MOVE){
            loadLevel(lev);
        }
    }

    public Vector getEmptyFields() {
        Vector fields = new Vector();

        int x1 = level.getActiveX1();
        int y1 = level.getActiveY1();
        int x2 = level.getActiveX2();
        int y2 = level.getActiveY2();

        if (level.getActiveX2() - level.getActiveX1() == 1){
            int y = level.getActiveY1();
            for (int i = level.getActiveY1(); i > 0; i--){
                y1--;
                y2--;
                if (isOverlapped(x1, y1, x2, y2)){
                    break;
                } else {
                    y = y1;
                }
            }

            fields.addElement(new Integer(y));

            y = level.getActiveY2();

            y1 = level.getActiveY1();
            y2 = level.getActiveY2();

            for (int i = level.getActiveY2(); i < size; i++){
                y1++;
                y2++;
                if (isOverlapped(x1, y1, x2, y2)){
                    break;
                } else {
                    y = y2;
                }
            }

            fields.addElement(new Integer(y));
        }

        if (level.getActiveY2() - level.getActiveY1() == 1){
            for (int i = 0; i < size; i++){
                int x = level.getActiveX1();

                for (int j = level.getActiveX1(); j > 0; j--){
                    x1--;
                    x2--;
                    if (isOverlapped(x1, y1, x2, y2)){
                        break;
                    } else {
                        x = x1;
                    }
                }

                fields.addElement(new Integer(x));

                x = level.getActiveX2();

                x1 = level.getActiveX1();
                x2 = level.getActiveX2();

                for (int j = level.getActiveX2(); j < size; j++){
                    x1++;
                    x2++;
                    if (isOverlapped(x1, y1, x2, y2)){
                        break;
                    } else {
                        x = x2;
                    }
                }

                fields.addElement(new Integer(x));
            }
        }

        return fields;
    }

    public boolean isGo() {
        return level.isGo();
    }

    private static interface IRules{
        boolean checkBorder(int i);
        boolean checkTube(int i);
        boolean checkRightHand(int i, int k);

        boolean ckeckNear(int i);

        boolean checkNearTube(int i);
    }
}
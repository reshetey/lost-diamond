package com.ximad.lostdiamond.logic;

import com.ximad.lostdiamond.ClipsPlayer;
import com.ximad.lostdiamond.DeskRenderer;
import com.ximad.lostdiamond.LevelFinishedScreen;
import com.ximad.lostdiamond.ad.CustomAd;
import com.ximad.lostdiamond.data.Kid;
import com.ximad.lostdiamond.data.Crazy;
import com.ximad.lostdiamond.data.Normal;
import com.ximad.lostdiamond.data.Pro;
import com.ximad.lostdiamond.data.Tier;
import com.ximad.lostdiamond.device.Dimension;
import com.ximad.lostdiamond.device.Dimension480360;
import com.ximad.lostdiamond.menu.Access;
import com.ximad.lostdiamond.menu.Data2;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.ui.UiApplication;

import java.util.Vector;

public class BlocksManager {
    private FTest ftest;

    private Dimension dimension;

    private DeskRenderer deskRenderer;

    private int activeLevel;

    private int activeTier;

    private static Vector tiers;
    static final long KEY = 0x9dc8f587ddc6d6b4L;
    private static PersistentObject persistentObject;
    private static Data2 data2;
    private boolean sound;
    private boolean music;

    private ClipsPlayer clipsPlayer;
    private int actualLevel;
    private int slide;

    public BlocksManager() {
        tiers = new Vector();
        tiers.addElement(new Kid());
        tiers.addElement(new Normal());
        tiers.addElement(new Pro());
        tiers.addElement(new Crazy());

        clipsPlayer = new ClipsPlayer();
        loadSounds();

        persistentObject = PersistentStore.getPersistentObject(KEY);
        data2 = (Data2) persistentObject.getContents();

        initData();

        ftest = new FTest();
//        dimension = new Dimension320240();
//        dimension = new Dimension360480();
//        dimension = new Dimension480320();
        dimension = new Dimension480360();

        deskRenderer = new DeskRenderer(this);
    }

    public Dimension getDimension() {
        return dimension;
    }

    private void loadSounds()
    {
/*
   if (System.getProperty("supports.mixing").equals("false"))   // always false on simulator
      System.out.println("mixing not supported on this device");
      return;
   }
 */
        clipsPlayer.load("brick-brick.wav");
        clipsPlayer.load("brick-box.wav");
        clipsPlayer.load("levelend.wav");
        clipsPlayer.load("background.mp3");
    }


    private void initData() {
        if (data2 == null){
            data2 = new Data2(tiers.size());
            data2.setSound(true);

            for (int i = 0; i < tiers.size(); i++){
                int size = ((Tier)tiers.elementAt(i)).getMin().length;

                data2.setPoints(i, new int[size]);
            }
        }

        music = data2.isMusic();
        sound = data2.isSound();

        activeTier = data2.getActiveTier();
    }

    public void saveData(){
        persistentObject.setContents(data2);
        persistentObject.commit();
    }


    public Access[] getLevels(){
        int points[] = data2.getPoints(activeTier);

        Access accesses[] = new Access[points.length];

        for (int i = 0; i < points.length; i++) {
            boolean res;
            res = (i == 0) || points[i - 1] != 0;
            accesses[i] = new Access(String.valueOf(i + 1), points[i], ((Tier)tiers.elementAt(activeTier)).getMin()[i], res);
        }

        return accesses;
    }

    public Access [] getTiers(){
        Access[] accesses = new Access[tiers.size()];

        for (int i = 0; i < tiers.size(); i++){
            Tier t = (Tier)tiers.elementAt(i);

            accesses[i] = new Access(t.getDescription(), i, 0, true);
        }

        return accesses;
    }

    public void setActiveLevel(int activeLevel) {
        this.activeLevel = activeLevel;
    }

    public void setActiveTier(int activeTier) {
        this.activeTier = activeTier;
        data2.setActiveTier(activeTier);
    }

    public void showLevel(){
        ftest.setTier((Tier)tiers.elementAt(activeTier));
        ftest.setPoints(data2.getPoints(activeTier));
        ftest.loadLevel(activeLevel);
        actualLevel = activeLevel;
        deskRenderer.initLevel(ftest.getFigures());
        if (music) {
            clipsPlayer.loop("background.mp3");
        }
        UiApplication.getUiApplication().pushScreen(deskRenderer);
        if (activeLevel != 0){
            CustomAd.show();
        }
    }

    public int getSize() {
        return ftest.getSize();
    }

    public int getMin() {
        return ftest.getMin();
    }

    public int getMove() {
        return ftest.getMove();
    }

    public int getPoint() {
        return ftest.getPoint();
    }

    public int getLevel() {
        return ftest.getLevel();
    }

    public void changeGo() {
        ftest.changeGo();

        if (ftest.isGo()){
            slide = 0;
        }
    }

    public boolean undo() {
        return ftest.undo();
    }

    public int getActive(){
        return ftest.getActive();
    }

    private void playSound(int value) {
        if (!sound){
            return;
        }

        if (value == -1 && slide != 0){
            clipsPlayer.play("brick-brick.wav");
        }

        if (value == -2 && slide != 0){
            clipsPlayer.play("brick-box.wav");
        }
    }

    public boolean left() {
        int value = ftest.left();
        playSound(value);
        if (reinitLevel()){
            return true;
        }

        countSlide(value);

        return value > 0;
    }

    public boolean up() {
        int value = ftest.up();
        playSound(value);
        if (reinitLevel()){
            return true;
        }

        countSlide(value);

        return value > 0;
    }

    public boolean down() {
        int value = ftest.down();
        playSound(value);
        if (reinitLevel()){
            return true;
        }

        countSlide(value);
        return value > 0;
    }
    public boolean right(){
        int value = ftest.right();

        boolean res = value > 0;

        if (actualLevel < ftest.getLevel()){
            actualLevel = ftest.getLevel();

            slide = 0;
            if (sound) {
                clipsPlayer.play("levelend.wav");
            }

            final LevelFinishedScreen levelFinishedScreen = new LevelFinishedScreen();

            UiApplication.getUiApplication().pushScreen(levelFinishedScreen);

            UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                    UiApplication.getUiApplication().popScreen(levelFinishedScreen);
                    CustomAd.show();
                    deskRenderer.initLevel(ftest.getFigures());
                }
            }, 1000, false);

            return false;
        }

        playSound(value);

        if (reinitLevel()){
            return true;
        }

        countSlide(value);
        return res;
    }

    private void countSlide(int value) {
        if (value > 0){
            slide = 1;
        } else {
            slide = 0;
        }
    }

    private boolean reinitLevel() {
        if (ftest.getMove() >= FTest.MAX_MOVE){
            ftest.loadLevel(actualLevel);
            deskRenderer.initLevel(ftest.getFigures());

            slide = 0;

            return true;
        }
        return false;
    }

    public void setSound(boolean checked) {
        sound = checked;
        data2.setSound(checked);
    }

    public void setMusic(boolean checked) {
        music = checked;
        data2.setMusic(checked);
    }

    public int getActiveTier(){
        return activeTier;
    }

    public String getActiveTierDescription(){
        return ((Tier)tiers.elementAt(activeTier)).getDescription();
    }

    public boolean setActive(float lastX, float lastY) {
        return ftest.setActive(lastX, lastY);
    }

    public void setGo(boolean b) {
        ftest.setGo(false);
    }

    public void saveTier() {
        data2.setPoints(activeTier, ftest.getPoints());

        if (music){
            clipsPlayer.stop("background.mp3");
        }
    }

    public boolean getSound() {
        return sound;
    }

    public boolean getMusic() {
        return music;
    }

    public Vector getEmptyFields() {
        return ftest.getEmptyFields();
    }

}
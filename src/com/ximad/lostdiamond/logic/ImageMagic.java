package com.ximad.lostdiamond.logic;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Graphics;

/**
 * Author: suzhakov
 * Date: May 19, 2011
 * Time: 2:32:05 PM
 */
public class ImageMagic {
    private static final Bitmap BITMAP_KID_LEVEL = Bitmap.getBitmapResource("kid.png");
    private static final Bitmap BITMAP_NORMAL_LEVEL = Bitmap.getBitmapResource("normal.png");
    private static final Bitmap BITMAP_PRO_LEVEL = Bitmap.getBitmapResource("pro.png");
    private static final Bitmap BITMAP_CRAZY_LEVEL = Bitmap.getBitmapResource("crazy.png");

    private static final Bitmap[] digits = new Bitmap[]{
            Bitmap.getBitmapResource("0.png"),
            Bitmap.getBitmapResource("1.png"),
            Bitmap.getBitmapResource("2.png"),
            Bitmap.getBitmapResource("3.png"),
            Bitmap.getBitmapResource("4.png"),
            Bitmap.getBitmapResource("5.png"),
            Bitmap.getBitmapResource("6.png"),
            Bitmap.getBitmapResource("7.png"),
            Bitmap.getBitmapResource("8.png"),
            Bitmap.getBitmapResource("9.png")
    };

    public static Bitmap getDefaultDigit(){
        Bitmap result = new Bitmap(51, 26);

        Graphics g = Graphics.create(result);

        return result;
    }

    public static Bitmap getDefaultLevel() {
        return BITMAP_KID_LEVEL;
    }

    public static Bitmap getLevelBitmap(int activeTier) {
        if (activeTier == 0){
            return BITMAP_KID_LEVEL;
        }

        if (activeTier == 1){
            return BITMAP_NORMAL_LEVEL;
        }

        if (activeTier == 2){
            return BITMAP_PRO_LEVEL;
        }

        if (activeTier == 3){
            return BITMAP_CRAZY_LEVEL;
        }

        return BITMAP_KID_LEVEL;
    }

    public static Bitmap getDigits(int number, int n) {
        String s = Integer.toString(number);

        Bitmap result = Bitmap.getBitmapResource("digit_bg" + n + ".jpg");

//        Bitmap result = new Bitmap(51, 26);

        Graphics g = Graphics.create(result);

        for (int j = 0; j < s.length(); j++){
            int index = Integer.parseInt(s.substring(j, j + 1));
            g.drawBitmap(j*17 + 17 * (3 - s.length())/2, 0, 17, 26, digits[index], 0, 0);
        }

        return result;
    }

}

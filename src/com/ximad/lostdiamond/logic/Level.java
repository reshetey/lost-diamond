package com.ximad.lostdiamond.logic;

import java.util.Vector;

public class Level {
    private Vector list;

    private Vector undoList;

    private boolean go;

    private int move;

    private Figure startFigure;

    private int active;

    private int undoSize;

    public Level() {
        list = new Vector();
        undoList = new Vector();

        move = 0;

        go = false;

        active = 0;

        undoSize = 5;
    }

    public void setCar(int arr[]){
        Figure f = new Figure();
        f.setX1(arr[0]);
        f.setY1(arr[1]);
        f.setX2(arr[2]);
        f.setY2(arr[3]);
        f.setColor("blue");
        f.setBorderColor("red");

        list.addElement(f);
    }

    public void clean(){
        active = 0;
        move = 0;
        startFigure = null;
        go = false;

        list.removeAllElements();
        undoList.removeAllElements();
    }

    public void setTruck(int[] truck) {
        Figure f = new Figure();
        f.setX1(truck[0]);
        f.setY1(truck[1]);
        f.setX2(truck[2]);
        f.setY2(truck[3]);
        f.setColor("white");
        f.setBorderColor(null);

        list.addElement(f);
    }

    public boolean isCar() {
        return "blue".equals(getElement(active).getColor());
    }

    public int getActiveX1() {
        return getElement(active).getX1();
    }

    public int getActiveX2() {
        return getElement(active).getX2();
    }

    public int getActiveY1() {
        return getElement(active).getY1();
    }

    public int getActiveY2() {
        return getElement(active).getY2();
    }

    public void setActiveY1(int y1) {
        getElement(active).setY1(y1);
    }

    public void setActiveY2(int y2) {
        getElement(active).setY2(y2);
    }

    public void setActiveX1(int x1) {
        getElement(active).setX1(x1);
    }

    public void setActiveX2(int x2) {
        getElement(active).setX2(x2);
    }

    private void addFigureToUndo(Figure f) {
        Figure figure = new Figure();
        figure.setX1(f.getX1());
        figure.setX2(f.getX2());
        figure.setY1(f.getY1());
        figure.setY2(f.getY2());

        undoList.addElement(figure);

        if (undoList.size() > undoSize * 2){
            undoList.removeElementAt(0);
        }
    }

    public void setUndoSize(int undoSize) {
        this.undoSize = undoSize;
    }

    public boolean isGo() {
        return go;
    }

    public boolean undo() {
        int size = undoList.size();

        if (size < 2){
            return false;
        }

        Figure actualFigure = (Figure)undoList.elementAt(size - 1);
        Figure targetFigure = (Figure)undoList.elementAt(size - 2);

        for (int i = 0; i < list.size(); i++) {
            Figure f = (Figure)list.elementAt(i);

            if (f.getX1() == actualFigure.getX1() && f.getX2() == actualFigure.getX2()&&
                f.getY1() == actualFigure.getY1() && f.getY2() == actualFigure.getY2()){
                f.setX1(targetFigure.getX1());
                f.setX2(targetFigure.getX2());
                f.setY1(targetFigure.getY1());
                f.setY2(targetFigure.getY2());

                move--;

                undoList.removeElementAt(size - 1);
                undoList.removeElementAt(size - 2);
                return true;
            }
        }
        return false;
    }

    public void setGo(boolean b){
        go = b;
        change();
    }

    private void change() {
        if (go){
            startFigure = new Figure();
            startFigure.setX1(getElement(active).getX1());
            startFigure.setX2(getElement(active).getX2());
            startFigure.setY1(getElement(active).getY1());
            startFigure.setY2(getElement(active).getY2());

            getElement(active).setBorderColor("green");
        }else{
            if (startFigure != null && (!checkEquals(startFigure, getElement(active)))){
                move++;
                addFigureToUndo(startFigure);
                addFigureToUndo(getElement(active));
            }
            startFigure = null;
            getElement(active).setBorderColor("red");
        }
    }

    private boolean checkEquals(Figure startFigure, Figure endFigure) {
        return startFigure.getX1() == endFigure.getX1() &&
               startFigure.getX2() == endFigure.getX2() &&
               startFigure.getY1() == endFigure.getY1() &&
               startFigure.getY2() == endFigure.getY2();
    }

    public void changeGo() {
        go = !go;

        change();
    }

    public void setActive(int k) {
        getElement(active).setBorderColor(null);
        active = k;
        getElement(active).setBorderColor("red");
    }

    public int getMove() {
        return move;
    }

    public boolean isActive(int i) {
        return active == i;
    }

    public int size(){
        return list.size();
    }

    public int getX1(int i) {
        return getElement(i).getX1();
    }

    private Figure getElement(int i) {
        return ((Figure)list.elementAt(i));
    }

    public int getX2(int i) {
        return getElement(i).getX2();
    }

    public int getY1(int i) {
        return getElement(i).getY1();
    }

    public int getY2(int i) {
        return getElement(i).getY2();
    }

    public Vector list() {
        return list;
    }

    public int getActive() {
        return active;
    }
}

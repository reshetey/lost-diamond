package com.ximad.lostdiamond;


//import com.transpera.sdk.blackberry.videoad.TransperaAd;
//import com.ximad.lostdiamond.ad.transpera.TransperaHandler;
import com.ximad.lostdiamond.logic.BlocksManager;
import com.ximad.lostdiamond.logic.Figure;
import com.ximad.lostdiamond.logic.ImageMagic;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;

import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import java.util.Hashtable;
import java.util.Vector;

/**
 * A screen which listens for touch events and draws lines that
 * follow the drawn path.
 */
public class DeskRenderer extends MainScreen /*implements TransperaAd.Listener*/{
    private final static Hashtable bitmaps = new Hashtable();

    private boolean visiblePopup = false;

    private static final Bitmap BITMAP_TRANSPARENT = Bitmap.getBitmapResource("2xhv.png");

    private static final Bitmap BITMAP_QUIT_YES = Bitmap.getBitmapResource("quit_to_main_yes.png");
    private static final Bitmap BITMAP_QUIT_NO = Bitmap.getBitmapResource("quit_to_main_no.png");

    private static final Bitmap BITMAP_QUIT_TO_MAIN = Bitmap.getBitmapResource("quit_to_main.png");

    private static final Bitmap DIAMOND_SELECTED = Bitmap.getBitmapResource("2xff.png");
    private static final Bitmap DIAMOND_MOVABLE = Bitmap.getBitmapResource("2xf.png");

    private BitmapField popup;

    private BitmapField yesNo;

    private int position = 1;

    private Vector figures = new Vector();

    static {
        bitmaps.put("bgh", Bitmap.getBitmapResource("bgh.jpg"));
        bitmaps.put("bgv", Bitmap.getBitmapResource("bgv.png"));
        bitmaps.put("blue", Bitmap.getBitmapResource("2xfff.png"));
        bitmaps.put("h2" ,Bitmap.getBitmapResource("2xh.png"));
        bitmaps.put("v2" ,Bitmap.getBitmapResource("2xv.png"));
        bitmaps.put("h3" ,Bitmap.getBitmapResource("3xh.png"));
        bitmaps.put("v3" ,Bitmap.getBitmapResource("3xv.png"));
        bitmaps.put("h2g",  Bitmap.getBitmapResource("2xhff.png"));
        bitmaps.put("h2r", Bitmap.getBitmapResource("2xhf.png"));
        bitmaps.put("h3g",  Bitmap.getBitmapResource("3xhff.png"));
        bitmaps.put("h3r",  Bitmap.getBitmapResource("3xhf.png"));
        bitmaps.put("v2g",  Bitmap.getBitmapResource("2xvff.png"));
        bitmaps.put("v2r",  Bitmap.getBitmapResource("2xvf.png"));
        bitmaps.put("v3g",  Bitmap.getBitmapResource("3xvff.png"));
        bitmaps.put("v3r",  Bitmap.getBitmapResource("3xvf.png"));
    }

    /** The x coordinate of the last visited point.*/
    private int _lastX = -1;

    /** The y coordinate of the last visited point.*/
    private int _lastY = -1;

    private int shift_x;
    private int shift_y;

    private int startX = -1;
    private int startY = -1;

    private BlocksManager blocksManager;

    private BitmapField[] bitmapFields = new BitmapField[20];
    private LayoutManager layoutManager;
    private int mScrWidth;
    private int mScrHeight;

    private BitmapField puzzleField = new BitmapField(null);

    private BitmapField bestField = new BitmapField(null);

    private BitmapField movesField = new BitmapField(null);

    private BitmapField minField = new BitmapField(null);

    private BitmapField levelField = new BitmapField(null);

    private Vector emptyFields = new Vector();
    private boolean ready;

    /**
     * Constructor.
     */
    public DeskRenderer(BlocksManager manager)
    {
        super(NO_VERTICAL_SCROLL);
        blocksManager = manager;

        layoutManager = new LayoutManager();
        add(layoutManager);

        shift_x = blocksManager.getDimension().getShiftX();
        shift_y = blocksManager.getDimension().getShiftY();
        String s = blocksManager.getDimension().getDirection();

        mScrWidth = blocksManager.getDimension().getWidth();
        mScrHeight = blocksManager.getDimension().getHeight();

        bitmapFields[0] = new BitmapField((Bitmap)bitmaps.get("bg" + s));
        layoutManager.add(bitmapFields[0]);

        layoutManager.add(puzzleField);
        layoutManager.add(bestField);
        layoutManager.add(movesField);
        layoutManager.add(minField);
        layoutManager.add(levelField);

        popup = new BitmapField(BITMAP_TRANSPARENT);

        layoutManager.add(popup);

        if (!blocksManager.getDimension().isTouchble()) {
            yesNo = new BitmapField(BITMAP_TRANSPARENT);
            layoutManager.add(yesNo);
        }
    }

    public int getSize(){
        return 6;
    }

    public Bitmap addRect(int x, int y, int x1, int y1, String color, String border){
        int dx = x1-x;
        int dy = y1-y;

        String im;
        if ("blue".equals(color)){
            im = "blue";
        } else{
            im = (dx == 1 ? "v" : "h") + Math.max(dx,dy);
        }

        if ("red".equals(border)){
            if ("blue".equals(color)){
                bitmapFields[layoutManager.size() + 1].setBitmap(DIAMOND_SELECTED);
            } else {
                bitmapFields[layoutManager.size() + 1].setBitmap((Bitmap)bitmaps.get(((dx == 1 ? "v" : "h") + Math.max(dx,dy) + "r")));
            }
        }

        if ("green".equals(border)){
            if ("blue".equals(color)){
                bitmapFields[layoutManager.size() + 1].setBitmap(DIAMOND_MOVABLE);
            } else {
                bitmapFields[layoutManager.size() + 1].setBitmap((Bitmap)bitmaps.get((dx == 1 ? "v" : "h") + Math.max(dx,dy) + "g"));
            }
        }
        return (Bitmap)bitmaps.get(im);
    }

    /**
     //     * @see net.rim.device.api.ui.Field#paint(Graphics)
     */
//    protected void paint(Graphics graphics)
//    {
//        super.paint(graphics);
//
//        graphics.setColor(visiblePopup? 0x8B4500 : 0xeec149);
//
//        Font f = getFont().derive(Font.BOLD, blocksManager.getDimension().getFontHeight());
//        graphics.setFont(f);
//
//        int w = getLabelWidth(f, "" + (blocksManager.getLevel() + 1));
//        graphics.drawText("" + (blocksManager.getLevel() + 1), blocksManager.getDimension().getLevelX(w), blocksManager.getDimension().getLevelY());
//        w = getLabelWidth(f, blocksManager.getActiveTierDescription());
//        graphics.drawText(blocksManager.getActiveTierDescription(), blocksManager.getDimension().getActiveTierX(w), blocksManager.getDimension().getActiveTierY());
//
//        w = getLabelWidth(f, "" + blocksManager.getPoint());
//        graphics.drawText("" + blocksManager.getPoint(), blocksManager.getDimension().getPointX(w), blocksManager.getDimension().getPointY());
//        w = getLabelWidth(f, "" + blocksManager.getMove());
//        graphics.drawText("" + blocksManager.getMove(), blocksManager.getDimension().getMoveX(w), blocksManager.getDimension().getMoveY());
//        w = getLabelWidth(f, "" + blocksManager.getMin());
//        graphics.drawText("" + blocksManager.getMin(), blocksManager.getDimension().getMinX(w), blocksManager.getDimension().getMinY());
//    }
//
//    private int getLabelWidth(Font font, String label) {
//        return font.getAdvance(label);
//    }

    public void close() {
        if (!visiblePopup){
            visiblePopup = true;

            position = 1;
            popup.setBitmap(BITMAP_QUIT_TO_MAIN);
            if (!blocksManager.getDimension().isTouchble()) {
                yesNo.setBitmap(BITMAP_QUIT_NO);
            }
            layoutManager.updateL2();
        }

    }


    protected boolean keyChar(char key, int i, int i1) {
        if (visiblePopup){
            return true;
        }

        if(key == 's') { //left
            refresh(blocksManager.left());
        } else if(key == 'f') { // right
            refresh(blocksManager.right());
        } else if(key == 'e') { // up
            refresh(blocksManager.up());
        } else if(key == 'x') { // down
            refresh(blocksManager.down());
        } else if(key == 'd') { // select figure
            blocksManager.changeGo();
            refresh(true);
        } else if(key == 'u') { // undo
            refresh(blocksManager.undo());
        }
        else {
            return false;
        }

        return true;
    }


/*
    protected boolean touchEvent(TouchEvent message)
    {
        // Retrieve the new x and y touch positions.
        int x = message.getX(1);
        int y = message.getY(1);

        int eventCode = message.getEvent();

        int side = blocksManager.getDimension().getSide();
        if(eventCode == TouchEvent.DOWN)
        {
            if (visiblePopup){
                if (x >= 160 && x <= 200 && y >=240 && y <=260){
                    position = 0;
                    if (!blocksManager.getDimension().isTouchble()) {
                        yesNo.setBitmap(BITMAP_QUIT_YES);
                    }
                    layoutManager.updateL2();
                }

                if (x >= 160 && x <= 200 && y >=280 && y <=300){
                    position = 1;
                    if (!blocksManager.getDimension().isTouchble()) {
                        yesNo.setBitmap(BITMAP_QUIT_NO);
                    }
                    layoutManager.updateL2();
                }

                return true;
            }

            if (blocksManager.setActive(((float)(x - shift_x))/side, ((float )(y - shift_y))/side)){
                _lastX = x;
                _lastY = y;

                startX = x;
                startY = y;

                Vector fields = blocksManager.getEmptyFields();

                for (int k = 0; k < fields.size(); k++){
                    emptyFields.addElement(fields.elementAt(k));
                }
            }
            else {
                _lastX = -1;
                _lastY = -1;

                startX = -1;
                startY = -1;
            }
        }

        if(eventCode == TouchEvent.MOVE)
        {
            if (!visiblePopup){
                if (_lastX == -1 && _lastY == -1){
                    return true;
                }

                boolean out = false;

                if (((Integer)emptyFields.elementAt(1)).intValue() == 6){
                    for (int n = 0; n < figures.size(); n++) {
                        Figure figure = (Figure)figures.elementAt(n);
                        if (figure.getBorderColor() != null && figure.getBorderColor().equals("green")
                                && figure.getColor().equals("blue")) {

                            if (figure.getX2() * side + (_lastX - startX) > 6 * side){
                                out = true;
                            }
                        }
                    }
                }

                if (out){
                    float diff = ((float)(_lastX - startX)) / side;

                    _lastX = -1;
                    _lastY = -1;

                    startX = -1;
                    startY = -1;

                    emptyFields.removeAllElements();

                    for (int k = 0; k < (int)Math.floor(diff + 0.5f) + 1; k++){
                        blocksManager.right();
                    }

                    return true;
                }

                _lastX = x;
                _lastY = y;

                refresh(true);
            }
        }

        if(eventCode == TouchEvent.UP)
        {
            // We have lost contact with the screen, reset the last x and y.

            if (visiblePopup){
                if (x >= 160 && x <= 200 && y >=240 && y <=260){
                    if (position == 0){
                        visiblePopup = false;
                        popup.setBitmap(BITMAP_TRANSPARENT);
                        if (!blocksManager.getDimension().isTouchble()) {
                            yesNo.setBitmap(BITMAP_TRANSPARENT);
                        }
                        blocksManager.saveTier();
                        super.close();
                    }
                }

                if (x >= 160 && x <= 200 && y >=280 && y <=300){
                    if (position == 1){
                        visiblePopup = false;

                        popup.setBitmap(BITMAP_TRANSPARENT);
                        if (!blocksManager.getDimension().isTouchble()) {
                            yesNo.setBitmap(BITMAP_TRANSPARENT);
                        }

                        layoutManager.updateL2();
                    }
                }

                return true;
            } else {
                if (_lastX != -1 && _lastY != -1){

                    float diff = 0;

                    int s = 0;

                    for (int n = 0; n < figures.size(); n++) {
                        Figure figure = (Figure)figures.elementAt(n);
                        if (figure.getBorderColor() != null && figure.getBorderColor().equals("green")) {
                            if (figure.getX2() - figure.getX1() == 1){
                                s = 1;
                            }
                            if (figure.getY2() - figure.getY1() == 1){
                                s = 2;
                            }
                        }
                    }

                    if (s == 1){
                        if (_lastY - startY < 0) {
                            diff = ((float)(startY - _lastY)) / side;

                            for (int k = 0; k < (int)Math.floor(diff + 0.5f); k++){
                                blocksManager.up();
                            }
                        }

                        if (_lastY - startY > 0) {
                            diff = ((float)(_lastY - startY)) / side;
                            for (int k = 0; k < (int)Math.floor(diff + 0.5f); k++){
                                blocksManager.down();
                            }
                        }
                    }
                    if (s == 2) {
                        if (_lastX - startX > 0) {
                            diff = ((float)(_lastX - startX)) / side;
                            for (int k = 0; k < (int)Math.floor(diff + 0.5f); k++){
                                blocksManager.right();
                            }
                        }
                    
                        if (_lastX - startX < 0) {
                            diff = ((float)(startX - _lastX)) / side;
                            for (int k = 0; k < (int)Math.floor(diff + 0.5f); k++){
                                blocksManager.left();
                            }
                        }
                    }
  
                    blocksManager.setGo(false);
                    refresh(true);
                }

            }

            emptyFields.removeAllElements();

            _lastX = -1;
            _lastY = -1;

            startX = -1;
            startY = -1;
        }

        return true;
   }
*/

    protected boolean invokeAction(int action) {

        switch(action)
        {
            case ACTION_INVOKE:{
                if (!visiblePopup){
                    keyChar('d', 0, 0);
                } else {
                    visiblePopup = false;
                    popup.setBitmap(BITMAP_TRANSPARENT);
                    if (!blocksManager.getDimension().isTouchble()) {
                        yesNo.setBitmap(BITMAP_TRANSPARENT);
                    }

                    if (position == 0){
                        blocksManager.saveTier();
                        super.close();
                    }

                    if (position == 1){
                        layoutManager.updateL2();
                    }
                }

                return true;
            }
        }
        return super.invokeAction(action);
    }

    protected boolean navigationMovement(int dx, int dy, int i2, int i3) {
        if (!visiblePopup) {
            if (dx < 0){
                keyChar('s', 0, 0);
            }else if (dx > 0){
                keyChar('f', 0, 0);
            }
            if (dy < 0){
                keyChar('e', 0, 0);
            }else if (dy > 0){
                keyChar('x', 0, 0);
            }

        } else {
            if (dy > 0 && position < 1){
                position++;
                if (!blocksManager.getDimension().isTouchble()) {
                    yesNo.setBitmap(BITMAP_QUIT_NO);
                }
                layoutManager.updateL2();
            }

            if (dy < 0 && position > 0){
                position--;
                if (!blocksManager.getDimension().isTouchble()) {
                    yesNo.setBitmap(BITMAP_QUIT_YES);
                }
                layoutManager.updateL2();
            }
        }

        return true;
    }

    public void refresh(boolean b){
        if (b){
            layoutManager.updateL();
        }
    }

    public void initLevel(Vector figures) {
        ready = false;
        layoutManager.reset();

        bitmapFields[figures.size() + 1] = new BitmapField((Bitmap)bitmaps.get("h2r"));

        layoutManager.setSize(figures.size());

        for (int i = 0; i < figures.size(); i++){
            Figure f = (Figure)figures.elementAt(i);
            bitmapFields[i + 1] = new BitmapField(addRect(f.getX1(), f.getY1(), f.getX2(), f.getY2(), f.getColor(), f.getBorderColor()));
            layoutManager.add(bitmapFields[i + 1], f);
        }

        layoutManager.add(bitmapFields[figures.size() + 1]);

        layoutManager.add(puzzleField);
        layoutManager.add(bestField);
        layoutManager.add(movesField);
        layoutManager.add(minField);
        layoutManager.add(levelField);

        layoutManager.add(popup);

        if (!blocksManager.getDimension().isTouchble()) {
            layoutManager.add(yesNo);
        }
        ready = true;
        layoutManager.updateL2();
    }

    /*public void onAdDownloaded(int result) {
        TransperaHandler.onAdDownloaded(result);
    }

    public void onAdInfoDownloaded(int i) {
        TransperaHandler.onAdInfoDownloaded(i);
    }*/

    class LayoutManager extends VerticalFieldManager {
        private int active = -1;
        private int size;

        public LayoutManager() {
            super(NO_VERTICAL_SCROLL|USE_ALL_HEIGHT | USE_ALL_WIDTH);
        }

        public void reset(){
            active = -1;
            figures.removeAllElements();

            for (int k = 1; k < bitmapFields.length; k++){
                if (bitmapFields[k] != null){
                    delete(bitmapFields[k]);
                    bitmapFields[k] = null;
                }
            }

            delete(puzzleField);
            delete(bestField);
            delete(movesField);
            delete(minField);
            delete(levelField);
            delete(popup);

            if (!blocksManager.getDimension().isTouchble()) {
                delete(yesNo);
            }

            active = blocksManager.getActive();
        }

        protected void sublayout(int width, int height) {
            if (!ready){
                return;
            }
            layoutChild(bitmapFields[0], blocksManager.getDimension().getWidth(), blocksManager.getDimension().getHeight());
            setPositionChild(bitmapFields[0], 0, 0);
            //means 50px for the touch
            int side = blocksManager.getDimension().getSide();

            for (int i = 0; i < figures.size(); i++){
                Field field = bitmapFields[i+1];
                Figure f = (Figure)figures.elementAt(i);

                int s_y = 0;
                int s_x = 0;

                if (blocksManager.getDimension().isTouchble() && f.getBorderColor() != null && f.getBorderColor().equals("green")){
                    if (f.getX2() - f.getX1() == 1){
                        if (_lastY - startY > 0){
                            s_y = Math.min((_lastY - startY),(((Integer)emptyFields.elementAt(1)).intValue() - f.getY2()) * side);
                        } else{
                            s_y = Math.max((_lastY - startY), -(f.getY1() - ((Integer)emptyFields.elementAt(0)).intValue()) * side);
                        }
                    }

                    if (f.getY2() - f.getY1() == 1){
                        if (_lastX - startX > 0){
                            s_x = Math.min((_lastX - startX),(((Integer)emptyFields.elementAt(1)).intValue() - f.getX2()) * side);
                        } else{
                            s_x = Math.max((_lastX - startX), - (f.getX1() - ((Integer)emptyFields.elementAt(0)).intValue()) * side);
                        }
                    }
                }

                layoutChild(field, (f.getX2()-f.getX1()) * blocksManager.getDimension().getSide(f.getX2()-f.getX1()), (f.getY2()-f.getY1()) * blocksManager.getDimension().getSide(f.getY2()-f.getY1()));
                setPositionChild(field, shift_x + f.getX1() * blocksManager.getDimension().getSide() + s_x, shift_y + f.getY1() * blocksManager.getDimension().getSide() + s_y);
            }

            int ac = blocksManager.getActive();
            Figure f1 = (Figure)figures.elementAt(ac);

            if (blocksManager.getDimension().isTouchble()){
                layoutChild(bitmapFields[figures.size() + 1], 0, 0);
            } else {
                layoutChild(bitmapFields[figures.size() + 1], (f1.getX2()-f1.getX1()) * blocksManager.getDimension().getSide((f1.getX2()-f1.getX1())), (f1.getY2()-f1.getY1()) * blocksManager.getDimension().getSide(f1.getY2()-f1.getY1()));
            }
            setPositionChild(bitmapFields[figures.size() + 1], shift_x + f1.getX1() * blocksManager.getDimension().getSide(), shift_y + f1.getY1() * blocksManager.getDimension().getSide());

            int x = blocksManager.getDimension().getWidth();
            int y = blocksManager.getDimension().getHeight();

            layoutLeftPanel();

            layoutChild(popup, x, y);
            setPositionChild(popup, 120, 80);

            if (!blocksManager.getDimension().isTouchble()) {
                layoutChild(yesNo, x, y);
                setPositionChild(yesNo, 120, 80);
            }

            setExtent(width, height);
        }

        private void layoutLeftPanel() {
            puzzleField.setBitmap(ImageMagic.getDigits(blocksManager.getLevel() + 1, 1));
            layoutChild(puzzleField, 100, 100);
            setPositionChild(puzzleField, 39, 43);

            bestField.setBitmap(ImageMagic.getDigits(blocksManager.getPoint(), 2));
            layoutChild(bestField, 100, 100);
            setPositionChild(bestField, 39, 111);

            movesField.setBitmap(ImageMagic.getDigits(blocksManager.getMove(), 3));
            layoutChild(movesField, 100, 100);
            setPositionChild(movesField, 39, 181);

            minField.setBitmap(ImageMagic.getDigits(blocksManager.getMin(), 4));
            layoutChild(minField, 100, 100);
            setPositionChild(minField, 39, 251);

            levelField.setBitmap(ImageMagic.getLevelBitmap(blocksManager.getActiveTier()));

            layoutChild(levelField, 200, 50);
            setPositionChild(levelField, 36, 320);
        }

        public int getPreferredWidth() {
            return mScrWidth;
        }

        public int getPreferredHeight() {
            return mScrHeight;
        }

        public void add(BitmapField bitmapField, Figure f) {
            figures.addElement(f);
            active = blocksManager.getActive();
            add(bitmapField);
        }

        public void updateL(){
            int ac = blocksManager.getActive();
            Figure f = (Figure)figures.elementAt(ac);

            Bitmap bitmap = addRect(f.getX1(), f.getY1(), f.getX2(), f.getY2(), f.getColor(), f.getBorderColor());
            bitmapFields[ac + 1].setBitmap(bitmap);

            Figure f2 = (Figure)figures.elementAt(active);
            Bitmap bitmap2 = addRect(f2.getX1(), f2.getY1(), f2.getX2(), f2.getY2(), f2.getColor(), f2.getBorderColor());
            bitmapFields[active + 1].setBitmap(bitmap2);

            active = ac;
            updateLayout();
        }

        public void updateL2() {
            updateLayout();
        }

        public int size() {
            return size;
        }

        public void setSize(int i) {
            size = i;
        }
    }
}
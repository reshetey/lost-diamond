package com.ximad.lostdiamond.menu;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;

public class RulesScreen extends MainScreen{
    private static final Bitmap BITMAP_SCREEN1 = Bitmap.getBitmapResource("instructions_1.jpg");
    private static final Bitmap BITMAP_SCREEN2 = Bitmap.getBitmapResource("instructions_2.jpg");
    private static final Bitmap BITMAP_SCREEN3 = Bitmap.getBitmapResource("instructions_3.jpg");

    private static final Bitmap BITMAP_MARK = Bitmap.getBitmapResource("instructions_mark.png");
    private static final Bitmap BITMAP_MARK_1 = Bitmap.getBitmapResource("instructions_mark_1.png");


    private BitmapField ruleField;

    private BitmapField markField1;
    private BitmapField markField2;
    private BitmapField markField3;

    private int position;

    public RulesScreen() {
        Manager manager = new Manager(Manager.USE_ALL_WIDTH | Manager.USE_ALL_HEIGHT) {

            protected void sublayout(int i, int i1) {
                layoutChild(ruleField, 480, 360);
                setPositionChild(ruleField, 0, 0);

                layoutChild(markField1, 40, 40);
                setPositionChild(markField1, 140, 320);

                layoutChild(markField2, 40, 40);
                setPositionChild(markField2, 220, 320);

                layoutChild(markField3, 40, 40);
                setPositionChild(markField3, 300, 320);

                setExtent(480, 360);
            }
        };

        ruleField = new BitmapField(BITMAP_SCREEN1);
        manager.add(ruleField);
        markField1 = new BitmapField(BITMAP_MARK);
        manager.add(markField1);
        markField2 = new BitmapField(BITMAP_MARK_1);
        manager.add(markField2);
        markField3 = new BitmapField(BITMAP_MARK_1);
        manager.add(markField3);

        add(manager);
    }

    protected boolean keyChar(char key, int i, int i1) {
        if(key == 's') { //left
            if (position > 0){
                position--;
                updateScreen();
            }
        }
        if(key == 'f') { // right
            if (position < 2) {
                position++;
                updateScreen();
            }
        }
        if (key == Characters.ESCAPE){
            UiApplication.getUiApplication().popScreen(this);
        }

        return true;
    }

    private void updateScreen() {
        if (position == 0){
            ruleField.setBitmap(BITMAP_SCREEN1);
            markField1.setBitmap(BITMAP_MARK);
            markField2.setBitmap(BITMAP_MARK_1);
            markField3.setBitmap(BITMAP_MARK_1);
        }
        if (position == 1){
            ruleField.setBitmap(BITMAP_SCREEN2);
            markField2.setBitmap(BITMAP_MARK);
            markField1.setBitmap(BITMAP_MARK_1);
            markField3.setBitmap(BITMAP_MARK_1);
        }
        if (position == 2){
            ruleField.setBitmap(BITMAP_SCREEN3);
            markField3.setBitmap(BITMAP_MARK);
            markField1.setBitmap(BITMAP_MARK_1);
            markField2.setBitmap(BITMAP_MARK_1);
        }

        updateLayout();
    }

    protected boolean navigationMovement(int dx, int dy, int i2, int i3) {
        if (dx < 0){
            keyChar('s', 0, 0);
        }else if (dx > 0){
            keyChar('f', 0, 0);
        }

        return super.navigationMovement(dx, dy, i2, i3);
    }

}
package com.ximad.lostdiamond.menu;

import com.ximad.lostdiamond.logic.BlocksManager;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;


public class TierScreen extends PopupScreen {

    private BlocksManager blocksManager;
    private BitmapField bitmapField;
    private BitmapField bitmapField2;
    private BitmapField bitmapField3;

    private int position = 0;

    public TierScreen(BlocksManager manager) {
        super(new VerticalFieldManager());
        setBackground(BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 0));

        blocksManager = manager;

        position = manager.getActiveTier();

        Manager sManager = new Manager(Manager.USE_ALL_WIDTH
                | Manager.USE_ALL_HEIGHT){

            protected void sublayout(int i, int i1) {
                layoutChild(bitmapField, blocksManager.getDimension().getTierExtentX(), blocksManager.getDimension().getTierExtentY());
                setPositionChild(bitmapField, 120, 60);

                layoutChild(bitmapField2, blocksManager.getDimension().getTierSelectorW(), blocksManager.getDimension().getTierSelectorH());

                setPositionChild(bitmapField2, blocksManager.getDimension().getTierSelectorX(), blocksManager.getDimension().getTierSelectorY() + position * blocksManager.getDimension().getTierSelectorGap());

                layoutChild(bitmapField3, blocksManager.getDimension().getTierExtentX(), blocksManager.getDimension().getTierExtentY());

                setPositionChild(bitmapField3, 120, 60);

                setExtent(480, 360);
            }
        };

        bitmapField = new BitmapField(Bitmap.getBitmapResource("popup.png"));
        sManager.add(bitmapField);

        bitmapField2 = new BitmapField(Bitmap.getBitmapResource("popup_layer.png"));
        sManager.add(bitmapField2);

        bitmapField3 = new BitmapField(Bitmap.getBitmapResource("popup_text.png"));
        sManager.add(bitmapField3);

        add (sManager);
    }
    // WARNING: removes black border
    protected void applyTheme(){
        
    }

//    protected void sublayout(int width, int height) {
//        setExtent(blocksManager.getDimension().getTierExtentX(), blocksManager.getDimension().getTierExtentY());
//
//        setPosition(blocksManager.getDimension().getTierPopupX(), blocksManager.getDimension().getTierPopupY());
//        setPosition(0, 0);
//
//        layoutDelegate(blocksManager.getDimension().getTierExtentX(), blocksManager.getDimension().getTierExtentY());
//    }


    protected boolean touchEvent(TouchEvent event) {
        // Retrieve the new x and y touch positions.
        int x = event.getX(1);
        int y = event.getY(1);

        int eventCode = event.getEvent();

        if(eventCode == TouchEvent.DOWN){
            if (x >= 20 && x <= 165 && y >=27 && y <= 77){
                position = 0;

                updateLayout();
            }

            if (x >= 20 && x <= 165 && y >=78 && y <=121){
                position = 1;

                updateLayout();
            }

            if (x >= 20 && x <= 165 && y >=122 && y <=164){
                position = 2;

                updateLayout();
            }

            if (x >= 20 && x <= 165 && y >=165 && y <=212){
                position = 3;

                updateLayout();
            }

        }

        if(eventCode == TouchEvent.UP){
            if (x >= 20 && x <= 165 && y >=27 && y <=78){
                if (position == 0){
                    invokeAction(ACTION_INVOKE);
                }
            }

            if (x >= 20 && x <= 165 && y >=78 && y <=121){
                if (position == 1){
                    invokeAction(ACTION_INVOKE);
                }
            }

            if (x >= 20 && x <= 165 && y >=122 && y <=164){
                if (position == 2){
                    invokeAction(ACTION_INVOKE);
                }
            }

            if (x >= 20 && x <= 165 && y >=165 && y <=212){
                if (position == 3){
                    invokeAction(ACTION_INVOKE);
                }
            }

        }

        return true;
    }

    protected boolean navigationMovement(int i, int i1, int i2, int i3) {
        if (i1 > 0 && position < 3){
            position++;
            updateLayout();
        }

        if (i1 < 0 && position > 0){
            position--;
            updateLayout();
        }

        return super.navigationMovement(i, i1, i2, i3);
    }

    protected boolean invokeAction(int action) {
        switch(action)
        {
            case ACTION_INVOKE:{

                blocksManager.setActiveTier(position);
                onClose();

                return true;
            }
        }
        return super.invokeAction(action);
    }

    public boolean onClose() {
        UiApplication.getUiApplication().popScreen(this);
        return true;
    }

    public void init() {
        Access access[] = blocksManager.getTiers();
        for (int i = 0; i < access.length; i++) {
            if (access[i].getDescription().equals(blocksManager.getActiveTierDescription())){
                position = i;
                break;
            }
            i++;
        }
    }
}
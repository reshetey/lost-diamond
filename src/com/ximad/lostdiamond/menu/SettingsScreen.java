package com.ximad.lostdiamond.menu;

import com.ximad.lostdiamond.logic.BlocksManager;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;


public class SettingsScreen extends PopupScreen {

    private BlocksManager blocksManager;

    private BitmapField bitmapField1;
    private BitmapField bitmapField2;
    private BitmapField bitmapField3;
//    private BitmapField bitmapField4;

    private SManager sManager;

    private int position = 0;

//    private boolean music = false;

    private boolean sound = false;

    private static final Bitmap BITMAP_SOUND = Bitmap.getBitmapResource("focus_sound.png");
    private static final Bitmap BITMAP_MUSIC = Bitmap.getBitmapResource("focus_music.png");
    private static final Bitmap BITMAP_OK = Bitmap.getBitmapResource("focus_ok.png");
    private static final Bitmap BITMAP_MARK = Bitmap.getBitmapResource("mark.png");
    private static final Bitmap BITMAP_MARK_BLANK = Bitmap.getBitmapResource("mark_tr.png");
    private static final Bitmap BITMAP_SETTINGS = Bitmap.getBitmapResource("music.png");

    private class SManager extends VerticalFieldManager{
        protected void sublayout(int i, int i1) {

            layoutChild(bitmapField1, 480, 360);

            setPositionChild(bitmapField1, 110, 60);

            if (!blocksManager.getDimension().isTouchble()) {
                if (position == 0){
                    bitmapField2.setBitmap(BITMAP_SOUND);
                }

                if (position == 1){
                    bitmapField2.setBitmap(BITMAP_MUSIC);
                }

                if (position == 2){
                    bitmapField2.setBitmap(BITMAP_OK);
                }
                layoutChild(bitmapField2, 480, 360);
                setPositionChild(bitmapField2, 110, 60);
            }

            if (sound){
                bitmapField3.setBitmap(BITMAP_MARK);
                layoutChild(bitmapField3, blocksManager.getDimension().getSettingsMarkW(), blocksManager.getDimension().getSettingsMarkH()/*50, 50*/);
            } else {
                bitmapField3.setBitmap(BITMAP_MARK_BLANK);
                layoutChild(bitmapField3, 98, 48);
            }

//            if (music){
//                bitmapField4.setBitmap(BITMAP_MARK);
//                layoutChild(bitmapField4, blocksManager.getDimension().getSettingsMarkW(), blocksManager.getDimension().getSettingsMarkH());
//            } else {
//                bitmapField4.setBitmap(BITMAP_MARK_BLANK);
//                layoutChild(bitmapField4, 98, 48);
//            }

            setPositionChild(bitmapField3, blocksManager.getDimension().getSettingsSoundMarkX() + 110, blocksManager.getDimension().getSettingsSoundMarkY() + 60);

//            setPositionChild(bitmapField4, blocksManager.getDimension().getSettingsMusicMarkX() + 110, blocksManager.getDimension().getSettingsMusicMarkY() + 60);

            setExtent(480, 360);
        }

        public void updateL() {
            updateLayout();
        }
    }

    public SettingsScreen(BlocksManager manager) {
        super(new VerticalFieldManager());
        setBackground(BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 0));
        blocksManager = manager;

        sound = blocksManager.getSound();

//        music = blocksManager.getMusic();

        sManager = new SManager();

        add (sManager);

        bitmapField1 = new BitmapField(BITMAP_SETTINGS);
        sManager.add(bitmapField1);

        bitmapField3 = new BitmapField(sound ? BITMAP_MARK : BITMAP_MARK_BLANK);
        sManager.add(bitmapField3);

//        bitmapField4 = new BitmapField(music ? BITMAP_MARK : BITMAP_MARK_BLANK);
//        sManager.add(bitmapField4);

        if (!blocksManager.getDimension().isTouchble()){
            bitmapField2 = new BitmapField(BITMAP_SOUND);
            sManager.add(bitmapField2);
        }
    }

    protected boolean navigationMovement(int i, int i1, int i2, int i3) {
        if (i1 > 0 && position < 2){
            position++;
            sManager.updateL();
        }

        if (i1 < 0 && position > 0){
            position--;
            sManager.updateL();
        }

        return super.navigationMovement(i, i1, i2, i3);
    }

    protected boolean invokeAction(int action) {
        switch(action)
        {
            case ACTION_INVOKE:{

                if (position == 0){
                    sound = !sound;
                    blocksManager.setSound(sound);
                    sManager.updateL();
                }

                if (position == 1){
//                    music = !music;
//                    blocksManager.setMusic(music);
                    //todo show instructions
                    UiApplication.getUiApplication().pushScreen(new RulesScreen());
//                    sManager.updateL();
                }

                if (position == 2){
                    onClose();
                }

                return true;
            }
        }
        return super.invokeAction(action);
    }

//    protected boolean touchEvent(TouchEvent event) {
//        // Retrieve the new x and y touch positions.
//        int x = event.getX(1);
//        int y = event.getY(1);
//
//        int eventCode = event.getEvent();
//
//        if(eventCode == TouchEvent.DOWN){
//            if (x >= 30 && x <= 215 && y >=30 && y <= 60){
//                position = 0;
//
//                sManager.updateL();
//            }
//
//            if (x >= 30 && x <= 215 && y >=75 && y <=115){
//                position = 1;
//
//                sManager.updateL();
//            }
//
//            if (x >= 90 && x <= 150 && y >=130 && y <=150){
//                position = 2;
//
//                sManager.updateL();
//            }
//
//        }
//
//        if(eventCode == TouchEvent.UP){
//            if (x >= 30 && x <= 215 && y >=30 && y <=60){
//                if (position == 0){
//                    invokeAction(ACTION_INVOKE);
//                }
//            }
//
//            if (x >= 30 && x <= 215 && y >=75 && y <=115){
//                if (position == 1){
//                    invokeAction(ACTION_INVOKE);
//                }
//            }
//
//            if (x >= 90 && x <= 150 && y >=130 && y <=150){
//                if (position == 2){
//                    invokeAction(ACTION_INVOKE);
//                }
//            }
//        }
//
//        return true;
//    }

    public boolean onClose() {
        UiApplication.getUiApplication().popScreen(this);
        return true;
    }

    protected void applyTheme() {
    }
}
package com.ximad.lostdiamond.menu;

import com.ximad.lostdiamond.ad.AdHandler;
import com.ximad.lostdiamond.ad.CustomAd;
import com.ximad.lostdiamond.ad.Consts_Ad;
import com.ximad.lostdiamond.ad.script.BannerPopupScreen;
import com.ximad.lostdiamond.component.CustomTextBox;
import com.ximad.lostdiamond.Utils;
import com.ximad.lostdiamond.logic.BlocksManager;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class BlocksScreen extends MainScreen {
    private BlocksManager blocksManager;
    private LevelScreen levelScreen;
    private TierScreen tierScreen;

    private SettingsScreen settingsScreen;
    private ConfirmScreen confirmScreen;

    private MManager mManager;

    private BitmapField menuField;
    private BitmapField upLayerField;

//    private int position = 3;
    private int position = 0;
    private static final Bitmap BITMAP_PUZZLES = Bitmap.getBitmapResource("puzzles_dark.png");
    private static final Bitmap BITMAP_SETS = Bitmap.getBitmapResource("sets_dark.png");
    private static final Bitmap BITMAP_SETTINGS = Bitmap.getBitmapResource("settings_dark.png");
    private static final Bitmap BITMAP_MENU = Bitmap.getBitmapResource("menu.jpg");
//    private BitmapField bannerField;
//    private CustomTextBox bannerTextBox;

    private class MManager extends VerticalFieldManager {

        protected void sublayout(int i, int i1) {
            layoutChild(menuField, blocksManager.getDimension().getWidth(), blocksManager.getDimension().getHeight());

            setPositionChild(menuField, 0, 0);


//            layoutChild(bannerTextBox, Consts_Ad.BANNER_DEFAULT_WIDTH, Consts_Ad.BANNER_DEFAULT_HEIGHT);
//            setPositionChild(bannerTextBox, (blocksManager.getDimension().getWidth() - Consts_Ad.BANNER_DEFAULT_WIDTH) / 2, blocksManager.getDimension().getBannerY());
//
//            layoutChild(bannerField, blocksManager.getDimension().getBannerWidth(), blocksManager.getDimension().getBannerHeight());
//            setPositionChild(bannerField, (blocksManager.getDimension().getWidth() - bannerField.getWidth()) / 2, blocksManager.getDimension().getBannerY());


            if (position == 0){
                upLayerField.setBitmap(BITMAP_PUZZLES);
            }

            if (position == 1){
                upLayerField.setBitmap(BITMAP_SETS);
            }

            if (position == 2){
                upLayerField.setBitmap(BITMAP_SETTINGS);
            }

            if (position == 3){
                upLayerField.setBitmap(BITMAP_SETTINGS);
            }

//            if (position == 3){
//                layoutChild(layerField, 0, 0);
//            } else {
//                layoutChild(layerField, blocksManager.getDimension().getBlocksSelectorW(), blocksManager.getDimension().getBlocksSelectorH());
//            }
//
//            setPositionChild(layerField, blocksManager.getDimension().getBlocksSelectorX(), blocksManager.getDimension().getBlocksSelectorY() + position * blocksManager.getDimension().getBlocksSelectorGap());
            if (position == 3){
                layoutChild(upLayerField, 0, 0);
            } else {
                layoutChild(upLayerField, blocksManager.getDimension().getWidth(), blocksManager.getDimension().getHeight());
            }

            setExtent(blocksManager.getDimension().getWidth(), blocksManager.getDimension().getHeight());
        }

    }

    public BlocksScreen() {
        blocksManager = new BlocksManager();
        tierScreen = new TierScreen(blocksManager);

        mManager = new MManager();

        confirmScreen = new ConfirmScreen(blocksManager);

        menuField = new BitmapField(BITMAP_MENU);

        mManager.add(menuField);

        upLayerField = new BitmapField(BITMAP_SETS);
//        layerField = new BitmapField(BITMAP_LAYER);

/*
        bannerTextBox = AdHandler.getBannerTextBox();

        bannerTextBox.setText(AdHandler.getBannerTextBox().getText());
        bannerTextBox.setVerticalScroll(0);

        bannerTextBox.setFontColor(Color.GRAY);

        bannerField = AdHandler.getBannerField();

        mManager.add(bannerTextBox);

        mManager.add(bannerField);
*/

//        mManager.add(layerField);
        mManager.add(upLayerField);

        settingsScreen = new SettingsScreen(blocksManager);

        levelScreen = new LevelScreen(blocksManager){
            public void showLevel() {
                blocksManager.showLevel();
            }
        };

        add(mManager);
    }

    /*protected boolean touchEvent(TouchEvent event) {
        // Retrieve the new x and y touch positions.
        int x = event.getX(1);
        int y = event.getY(1);

        int eventCode = event.getEvent();

        if(eventCode == TouchEvent.DOWN){
            if (x >= 100 && x <= 250 && y >=105 && y <=155){
                position = 0;

                updateLayout();

            }

            if (x >= 100 && x <= 250 && y >=170 && y <=220){
                position = 1;

                updateLayout();

            }

            if (x >= 100 && x <= 250 && y >=240 && y <=290){
                position = 2;

                updateLayout();
            }
        }

        if(eventCode == TouchEvent.UP){
            if (x >= 100 && x <= 250 && y >=105 && y <=155){
                if (position == 0){
                    invokeAction(ACTION_INVOKE);
                }
            }

            if (x >= 100 && x <= 250 && y >=170 && y <=220){
                if (position == 1){
                    invokeAction(ACTION_INVOKE);
                }
            }

            if (x >= 100 && x <= 250 && y >=240 && y <=290){
                if (position == 2){
                    invokeAction(ACTION_INVOKE);
                }
            }

        }

        if(eventCode == TouchEvent.CLICK){
             if (x >= 30 && x <= 330 && y >=360 && y <=410){
                 showAd();
            }
        }

        return true;
    }


    private void showAd() {
        CustomAd.clickPopupAction();
        AdHandler.adClick(AdHandler.getClickLink());
    }
*/
    protected boolean navigationMovement(int i, int i1, int i2, int i3) {
//        if (i1 > 0 && position < 3){
        if (i1 > 0 && position < 2){
            position++;

            updateLayout();
        }

        if (i1 < 0 && position > 0){
            position--;

            updateLayout();
        }

        return super.navigationMovement(i, i1, i2, i3);
    }

    protected boolean invokeAction(int action) {
        switch(action)
        {
            case ACTION_INVOKE:{

                if (position == 0){
                    levelScreen.init();
                    UiApplication.getUiApplication().pushScreen(levelScreen);
//                    position = 3;
                    updateLayout();
                    return true;
                }

                if (position == 1){
                    tierScreen.init();
                    UiApplication.getUiApplication().pushScreen(tierScreen);
                }

                if (position == 2){
                    UiApplication.getUiApplication().pushScreen(settingsScreen);
                }

      /*          if (position == 3){
                    showAd();
                }*/

                return true;
            }
        }
        return super.invokeAction(action);
    }


    public void close() {
        UiApplication.getUiApplication().pushModalScreen(confirmScreen);

        if(confirmScreen.getResult() == ConfirmScreen.YES){
            blocksManager.saveData();
            super.close();
        }

    }

    protected boolean onSavePrompt() {
        return true;
    }
}
package com.ximad.lostdiamond.menu;

//import com.transpera.sdk.blackberry.videoad.TransperaAd;
//import com.ximad.lostdiamond.ad.transpera.TransperaHandler;

import com.ximad.lostdiamond.logic.BlocksManager;
import com.ximad.lostdiamond.logic.ImageMagic;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LevelScreen extends MainScreen /*implements TransperaAd.Listener*/ {
    private static final int IMAGES_PER_SCREEN = 15;

    private int imagesInRow;

    private int imageWidth;
    private int imageHeight;

    private int gapX;
    private int gapY;

    private int shiftY;

    private int arrowWidth;
    private int arrowHeight;

    private BlocksManager blocksManager;
    private boolean ready;

    private LManager mainManager;
    private int shift = 0;
    private int size = 15;

    private int lastX = -1;
    private int lastY = -1;


    private static final Bitmap BITMAP_PASSED = Bitmap.getBitmapResource("passed.png");

    private static final Bitmap BITMAP_LOCKED = Bitmap.getBitmapResource("locked.png");

    private static final Bitmap BITMAP_MEDAL = Bitmap.getBitmapResource("medal.png");

/*
    public void onAdDownloaded(int result) {
        TransperaHandler.onAdDownloaded(result);
    }

    public void onAdInfoDownloaded(int i) {
        TransperaHandler.onAdInfoDownloaded(i);
    }
*/

    private class LManager extends VerticalFieldManager{
        private BitmapField [] buttons;
        private BitmapField bitmapField = new BitmapField(Bitmap.getBitmapResource("puzzles.jpg"));

        private BitmapField levelField = new BitmapField(ImageMagic.getDefaultLevel());

        private BitmapField scrollerField = new BitmapField(Bitmap.getBitmapResource("slider.png"));

        private BitmapField focusField = new BitmapField(Bitmap.getBitmapResource("focus.png"));

        private int focusPosition;

        public LManager(long style) {
            super(style);
            add (bitmapField);

            add (levelField);

            add (scrollerField);

            buttons = new BitmapField[IMAGES_PER_SCREEN];

            for (int i = 0; i < IMAGES_PER_SCREEN; i++){
                buttons[i] = new BitmapField(BITMAP_PASSED);
                add(buttons[i]);
            }

            add (focusField);
        }

        protected void sublayout(int width, int height) {
            if (!ready){
                return;
            }

            int displayWidth = blocksManager.getDimension().getWidth();
            int displayHeight = blocksManager.getDimension().getHeight();

            layoutChild(bitmapField, displayWidth, displayHeight);
            setPositionChild(bitmapField, 0, 0);

            levelField.setBitmap(ImageMagic.getLevelBitmap(blocksManager.getActiveTier()));

            layoutChild(levelField, 56, 18);
            setPositionChild(levelField, 275, 35);

            Access access[] = blocksManager.getLevels();

            layoutChild(scrollerField, 35, 34);
            setPositionChild(scrollerField, 444, 78 + shift * 220 /(access.length - IMAGES_PER_SCREEN));

            for (int j = 0; j < IMAGES_PER_SCREEN; j++){
                BitmapField field = buttons[j];

                if (j + shift >= access.length){
                    field.setBitmap(BITMAP_PASSED);
                    continue;
                }

                if (access[j + shift].isAccess()){
                    if (access[j + shift].getBest() != access[j + shift].getMin()){
                        field.setBitmap(BITMAP_PASSED);
                    } else {
                        field.setBitmap(BITMAP_MEDAL);
                    }
                } else {
                    field.setBitmap(BITMAP_LOCKED);
                }

                layoutChild(field, imageWidth, imageHeight);
                setPositionChild(field, j% imagesInRow * imageWidth + 12, j/ imagesInRow * imageHeight + 82);
            }

            if (blocksManager.getDimension().isTouchble()){
                layoutChild(focusField, 0, 0);
            } else {
                layoutChild(focusField, imageWidth, imageHeight);
            }

            setPositionChild(focusField, (focusPosition - shift) % imagesInRow * imageWidth + 12, (focusPosition - shift) / imagesInRow * imageHeight + 82);

            setExtent(displayWidth, displayHeight);
        }

        public void setFocusPosition(int focusPosition) {
            this.focusPosition = focusPosition;
        }

        public int getFocusPosition() {
            return focusPosition;
        }

        protected void paint(Graphics graphics) {
            super.paint(graphics);

            Access access[] = blocksManager.getLevels();

            for (int j = 0; j < IMAGES_PER_SCREEN; j++){
                if (j + shift >= access.length){
                    continue;
                }
                String _label = "" + (j + 1 + shift);
                Font f = getFont().derive(Font.BOLD, blocksManager.getDimension().getLevelFont());

                graphics.setFont(f);

                int _labelHeight = f.getHeight();
                int _labelWidth = f.getAdvance(_label);

                graphics.drawText(_label, j% imagesInRow * imageWidth + 12 + (imageWidth - _labelWidth) / 2, j/ imagesInRow * imageHeight + (imageWidth - _labelHeight)/2 + 82);

                Font f1 = getFont().derive(Font.PLAIN, blocksManager.getDimension().getLevelMinFont());

                graphics.setFont(f1);

                if (access[j + shift].isAccess()){
                    int w1 = f1.getAdvance("" + access[j + shift].getBest());
                    int w2 = f1.getAdvance("" + access[j + shift].getMin());

                    graphics.drawText("" + access[j + shift].getBest(), j% imagesInRow * imageWidth + 12 + blocksManager.getDimension().getLevelBestX(w1),
                            j/ imagesInRow * imageHeight + 92);

                    graphics.drawText("" + access[j + shift].getMin(), j% imagesInRow * imageWidth + 12 + blocksManager.getDimension().getLevelMinX(w2),
                            j/ imagesInRow * imageHeight + 92);
                }
            }
        }

        protected boolean isDownArrowShown() {
            return shift + IMAGES_PER_SCREEN < size;
        }

        protected boolean isUpArrowShown() {
            return shift > 0;
        }

        public void uL(){
            updateLayout();
        }

    }
                                                 
    public LevelScreen(BlocksManager blocksManager) {
        super(Manager.NO_VERTICAL_SCROLL);

        mainManager = new LManager(Manager.NO_VERTICAL_SCROLL);

        add(mainManager);

        imagesInRow = blocksManager.getDimension().getLevelImagesInRow();
        imageWidth = blocksManager.getDimension().getLevelImageWidth();
        imageHeight = blocksManager.getDimension().getLevelImageHeight();

        gapX = blocksManager.getDimension().getLevelImageGapX();
        gapY = blocksManager.getDimension().getLevelImageGapY();

        arrowWidth = blocksManager.getDimension().getLevelArrowWidth();
        arrowHeight = blocksManager.getDimension().getLevelArrowHeight();

        shiftY = blocksManager.getDimension().getLevelShiftY();

        this.blocksManager = blocksManager;
    }

    public void showLevel(){

    }


    /*protected boolean touchEvent(TouchEvent event) {
        int code = event.getEvent();

        int x = event.getX(1);
        int y = event.getY(1);

        if (code == TouchEvent.MOVE){
            if (y > lastY){
                navigationMovement(0, -1, 0, 0);
            }

            if (y < lastY){
                navigationMovement(0, 1, 0, 0);
            }

            lastX = x;
            lastY = y;
        }

        if (TouchEvent.DOWN == code){
            lastX = x;
            lastY = y;
        }

        if (TouchEvent.UP == code){
            if ((lastX != -1 && Math.abs(x - lastX) < 10 && (lastY != -1 && Math.abs(lastY - y) < 10))){
                int x1 = -1;
                for (int k = 0; k < imagesInRow; k++){
                    if (x >= gapX * (k + 1) + k * blocksManager.getDimension().getLevelImageWidth() &&
                            x <= gapX * (k + 1) + (k + 1) * blocksManager.getDimension().getLevelImageWidth()){
                        x1 = k;
                    }
                }

                int y1 = -1;
                for (int n = 0; n < imagesInRow; n++){
                    if (y >= gapY * (n + 1) + n * blocksManager.getDimension().getLevelImageHeight() + shiftY &&
                            y <= gapY * (n + 1) + (n + 1) * blocksManager.getDimension().getLevelImageHeight() + shiftY){
                        y1 = n;
                    }
                }

                if (x1 != -1 && y1 != -1){
                    int position = x1 + y1 * imagesInRow + shift;

                    showActive(position);
                }
            }

            lastX = -1;
            lastY = -1;
        }

        return super.touchEvent(event);
    }*/

    public boolean onClose() {
        UiApplication.getUiApplication().popScreen(this);
        return true;
    }

    protected boolean navigationMovement(int i, int i1, int i2, int i3) {

        if (i > 0 && mainManager.getFocusPosition() < size - 1){
            int position = mainManager.getFocusPosition();

            if ((position - shift) % IMAGES_PER_SCREEN == (IMAGES_PER_SCREEN - 1)){
                shift += imagesInRow;
            }
            mainManager.setFocusPosition(mainManager.getFocusPosition() + 1);
            updateLayout();
        }

        if (i < 0 && mainManager.getFocusPosition() > 0){
            int position = mainManager.getFocusPosition();

            if (position - shift == 0){
                shift -= imagesInRow;
            }

            mainManager.setFocusPosition(mainManager.getFocusPosition() - 1);

            updateLayout();
        }

        if (i1 > 0 && mainManager.getFocusPosition() + imagesInRow < size){
            int position = mainManager.getFocusPosition();

            if ((position - shift) % IMAGES_PER_SCREEN >= IMAGES_PER_SCREEN - imagesInRow){
                shift += imagesInRow;
            }
            mainManager.setFocusPosition(mainManager.getFocusPosition() + imagesInRow);
            updateLayout();
        }

        if (i1 < 0 && mainManager.getFocusPosition() - imagesInRow  >= 0){
            int position = mainManager.getFocusPosition();

            if ((position - shift) % IMAGES_PER_SCREEN < imagesInRow){
                shift -= imagesInRow;
            }
            mainManager.setFocusPosition(mainManager.getFocusPosition() - imagesInRow);
            updateLayout();
        }

        return super.navigationMovement(i, i1, i2, i3);
    }

    protected boolean invokeAction(int action) {
        switch(action)
        {
            case ACTION_INVOKE:{
                if (!blocksManager.getDimension().isTouchble()){
                    showActive(mainManager.getFocusPosition());
                }

                return true;
            }
        }
        return super.invokeAction(action);
    }

    private void showActive(int position) {
        Access access[] = blocksManager.getLevels();

        if (access[position].isAccess()){
            blocksManager.setActiveLevel(position);
            UiApplication.getUiApplication().popScreen(LevelScreen.this);
            showLevel();
        }
    }

    public void init() {
        ready = false;
        Access access[] = blocksManager.getLevels();

        for (int i = 0; i < access.length; i++){
            if (access[i].getBest() == 0 && access[i].isAccess()){
                mainManager.setFocusPosition(i);
                countShift();
                break;
            }
        }

        size = access.length;

        ready = true;

        mainManager.uL();
    }

    void countShift(){
        int position = mainManager.getFocusPosition();

        if (position < IMAGES_PER_SCREEN){
            shift = 0;
        } else {
            shift = ((position - IMAGES_PER_SCREEN) / imagesInRow  + 1) * imagesInRow;
        }
    }
}
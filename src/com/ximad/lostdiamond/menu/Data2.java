package com.ximad.lostdiamond.menu;

import net.rim.device.api.util.Persistable;

public class Data2 implements Persistable {
    private int points[][];
    private boolean isMusic;
    private boolean isSound;
    private int activeTier;

    public Data2() {
    }

    public Data2(int size) {
        points = new int[size][];

    }

    public int[] getPoints(int i) {
        return points[i];
    }

    public void setPoints(int i, int[] points) {
        this.points[i] = points;
    }

    public boolean isMusic() {
        return isMusic;
    }

    public void setMusic(boolean music) {
        isMusic = music;
    }

    public boolean isSound() {
        return isSound;
    }

    public void setSound(boolean sound) {
        isSound = sound;
    }

    public int getActiveTier() {
        return activeTier;
    }

    public void setActiveTier(int activeTier) {
        this.activeTier = activeTier;
    }
}

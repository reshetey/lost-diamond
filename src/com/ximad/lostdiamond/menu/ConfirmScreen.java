package com.ximad.lostdiamond.menu;

import com.ximad.lostdiamond.logic.BlocksManager;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;

public class ConfirmScreen extends PopupScreen {
    private BlocksManager blocksManager;

    private BitmapField bitmapField1;
    private BitmapField bitmapField2;

    private SManager sManager;

    private int result = NO;

    private int position = 1;

    private static final Bitmap BITMAP_QUIT = Bitmap.getBitmapResource("quit.png");
    private static final Bitmap BITMAP_QUIT_YES = Bitmap.getBitmapResource("quit_yes.png");
    private static final Bitmap BITMAP_QUIT_NO = Bitmap.getBitmapResource("quit_no.png");

    public static final int YES = 1;
    public static final int NO = 2;

    private class SManager extends VerticalFieldManager {
        protected void sublayout(int i, int i1) {

            layoutChild(bitmapField1, 480, 360);

            setPositionChild(bitmapField1, 100, 60);

            if (!blocksManager.getDimension().isTouchble()) {
                layoutChild(bitmapField2, blocksManager.getDimension().getConfirmExtentX(), blocksManager.getDimension().getConfirmExtentY());

                if (position == 0){
                    bitmapField2.setBitmap(BITMAP_QUIT_YES);
                }

                if (position == 1){
                    bitmapField2.setBitmap(BITMAP_QUIT_NO);
                }

                setPositionChild(bitmapField2, 100, 60);
            }

            setExtent(480, 360);
        }

        public void updateL() {
            updateLayout();
        }
    }

    public ConfirmScreen(BlocksManager blocksManager) {
        super(new VerticalFieldManager());
        this.setBackground(BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 0));
        this.blocksManager = blocksManager;

        sManager = new SManager();

        add (sManager);

        bitmapField1 = new BitmapField(BITMAP_QUIT);
        sManager.add(bitmapField1);

        if (!blocksManager.getDimension().isTouchble()) {
            bitmapField2 = new BitmapField(BITMAP_QUIT_YES);
            sManager.add(bitmapField2);
        }
    }

//    protected void sublayout(int i, int i1) {
//        setExtent(blocksManager.getDimension().getConfirmExtentX(), blocksManager.getDimension().getConfirmExtentY());
//
//        setPosition(blocksManager.getDimension().getConfirmPositionX(), blocksManager.getDimension().getConfirmPositionY());
//
//        layoutDelegate(blocksManager.getDimension().getConfirmExtentX(), blocksManager.getDimension().getConfirmExtentY());
//    }

    protected boolean navigationMovement(int i, int i1, int i2, int i3) {
        if (i1 > 0 && position < 1){
            position++;
            sManager.updateL();
        }

        if (i1 < 0 && position > 0){
            position--;
            sManager.updateL();
        }

        return super.navigationMovement(i, i1, i2, i3);
    }

    protected boolean invokeAction(int action) {
        switch(action)
        {
            case ACTION_INVOKE:{

                if (position == 0){
                    result = YES;
                    onClose();
                }

                if (position == 1){
                    result = NO;
                    onClose();
                }

                return true;
            }
        }
        return super.invokeAction(action);
    }

    public boolean onClose() {
        UiApplication.getUiApplication().popScreen(this);
        return true;
    }

    public int getResult() {
        return result;
    }

    protected boolean touchEvent(TouchEvent event) {
        // Retrieve the new x and y touch positions.
        int x = event.getX(1);
        int y = event.getY(1);

        int eventCode = event.getEvent();

        if(eventCode == TouchEvent.DOWN){
            if (x >= 100 && x <= 140&& y >=95 && y <=115){
                position = 0;
                sManager.updateL();
            }

            if (x >= 100 && x <= 140&& y >=135 && y <=155){
                position = 1;
                sManager.updateL();
            }

        }

        if(eventCode == TouchEvent.UP){
            if (x >= 100 && x <= 140&& y >=95 && y <=115){
                if (position == 0){
                    invokeAction(ACTION_INVOKE);
                }
            }

            if (x >= 100 && x <= 140&& y >=135 && y <=155){
                if (position == 1){
                    invokeAction(ACTION_INVOKE);
                }
            }

        }

        return true;
    }


    protected void applyTheme() {
    }
}
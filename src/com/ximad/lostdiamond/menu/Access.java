package com.ximad.lostdiamond.menu;

public class Access {
    private String description;
    private int best;
    private int min;
    private boolean access;

    public Access(String description, int best, int min, boolean access) {
        this.description = description;
        this.best = best;
        this.access = access;
        this.min = min;
    }

    public String getDescription() {
        return description;
    }

    public int getBest() {
        return best;
    }

    public boolean isAccess() {
        return access;
    }

    public int getMin() {
        return min;
    }
}

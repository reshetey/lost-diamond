package com.ximad.lostdiamond.component;

import com.ximad.lostdiamond.Utils;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class CustomButton extends Field implements DrawStyle {

	private Bitmap currentPicture = null;
	private Bitmap onPicture = null;
	private Bitmap offPicture = null;
	private Bitmap focusPicture = null;
	private Bitmap checkPicture = null;

	private int width;
	private int height;
	
	private boolean isFocusImageNeeded;
	private boolean isCheckImageNeeded;
	
	private boolean isFocused = false;
	
	private boolean checkState;

	public CustomButton(String offImagePath, String onImagePath,
			String focusImagePath, String checkImagePath) {
		super(Field.EDITABLE);
		
		if (offImagePath != null) {
			offPicture = Bitmap.getBitmapResource(offImagePath);
			
			width = offPicture.getWidth();
			height = offPicture.getHeight();
			
			currentPicture = offPicture;
		}
		
		if (onImagePath != null) {
			onPicture = Bitmap.getBitmapResource(onImagePath);
		}
		
		if (offPicture == null) {
			//Utils.displayAlert("�� ������� ����������� " + offImagePath);
		} else if (onPicture == null) {
			//Utils.displayAlert("�� ������� ����������� " + onImagePath);
		}
		
		
		if (focusImagePath == null) {
			isFocusImageNeeded = false;
		} else {
			focusPicture = Bitmap.getBitmapResource(focusImagePath);
			isFocusImageNeeded = true;
			
			width = focusPicture.getWidth();
			height = focusPicture.getHeight();
		}
		
		if (checkImagePath == null) {
			isCheckImageNeeded = false;
		} else {
			checkPicture = Bitmap.getBitmapResource(checkImagePath);
			isCheckImageNeeded = true;
		}
		
		checkState = false;
	}
	
	public void repaint() {
		invalidate();
	}

	public int getPreferredHeight() {
		return height;
	}

	public int getPreferredWidth() {
		return width;
	}

	public boolean isFocusable() {
		return isEditable();
	}

	protected void onFocus(int direction) {
		currentPicture = onPicture;
		invalidate();
	}

	protected void onUnfocus() {
		currentPicture = offPicture;
		invalidate();
	}

	protected void layout(int _width, int _height) {
		setExtent(Math.min(_width, getPreferredWidth()), Math.min(_height,
				getPreferredHeight()));
	}

	//update the fieldchange
	protected void fieldChangeNotify(int context) {
		try {
			this.getChangeListener().fieldChanged(this, context);
		} catch (Exception e) {
		}
	}
	
	protected void drawFocus(Graphics g, boolean on) {
		isFocused = true;
		
		if (getCheckState() == false)
			if (offPicture != null)
				g.drawBitmap(0, 0, getWidth(), getHeight(), offPicture, 0, 0);
		
        if (isFocusImageNeeded) {
        	if (focusPicture != null)
        		g.drawBitmap(0, 0, getWidth(), getHeight(), focusPicture, 0, 0);
        }
        
        if (currentPicture != null) { 
			g.drawBitmap(0, 0, getWidth(), getHeight(),
				currentPicture, 0, 0);
		}
    }
	
	public boolean getFocusState() {
		return isFocused;
	}
	
	public void setOffImage(String offImagePath) {
		offPicture = Bitmap.getBitmapResource(offImagePath);
		
		if (!getFocusState()) {
			currentPicture = offPicture;
		}
		
		invalidate();
	}

	public void setOffImageBitmap(Bitmap offImagePath) {
		offPicture = offImagePath;
		
		if (!getFocusState()) {
			currentPicture = offPicture;
		}
		
		invalidate();
	}

	public void setOnImage(String onImagePath) {
		onPicture = Bitmap.getBitmapResource(onImagePath);
		
		if (getFocusState()) {
			currentPicture = onPicture;
		}
		
		invalidate();
	}

	public void setOnImageBitmap(Bitmap onImagePath) {
		onPicture = onImagePath;
		
		if (getFocusState()) {
			currentPicture = onPicture;
		}
		
		invalidate();
	}

	public void setFocusImage(String focusImagePath) {
		
		if (focusImagePath == null) {
			isFocusImageNeeded = false;
		} else {
			isFocusImageNeeded = true;
			focusPicture = Bitmap.getBitmapResource(focusImagePath);
		}
		
		invalidate();
	}
	
	public void setCheckState(boolean state) {
		checkState = state;
		invalidate();
	}
	
	public void setSize(int _width, int _height) {
		width = _width;
		height = _height;
	}
	
	public boolean getCheckState() {
		return checkState;
	}

	public int getOffImageWidth() {
		return offPicture.getWidth();
	}
	
	protected void paint(Graphics g) {
		isFocused = false;
		
		if (currentPicture != null) { 
			g.drawBitmap(0, 0, getWidth(), getHeight(),
				currentPicture, 0, 0);
		}
		
		if (isCheckImageNeeded && checkState) {
			if (checkPicture != null)
			g.drawBitmap(0, 0, getWidth(), getHeight(),
					checkPicture, 0, 0);
		}
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(1);
		return true;
	}
}

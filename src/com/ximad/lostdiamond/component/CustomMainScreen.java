package com.ximad.lostdiamond.component;

import com.ximad.lostdiamond.Utils;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;

public abstract class CustomMainScreen extends MainScreen {
	
    CustomMainScreen prevScreen = null;
    
    public CustomMainScreen() {
    	super(NO_VERTICAL_SCROLL | DEFAULT_MENU | DEFAULT_CLOSE);
    }
    
    public CustomMainScreen(long style) {
    	super(style | NO_VERTICAL_SCROLL | DEFAULT_MENU | DEFAULT_CLOSE);
    }

    public void openScreen(CustomMainScreen nextScreen) {
    	
		nextScreen.prevScreen = this;
		
        try {
        	UiApplication.getUiApplication().popScreen(this);
        } catch(IllegalArgumentException ex) {
//        	Utils.showMessage("screen is not on the stack!");
        }
         
        UiApplication.getUiApplication().pushScreen(nextScreen);
    }

    public void openPrevScreen() {
    	try {   		
    		UiApplication.getUiApplication().popScreen(this);
    		
    		if (prevScreen != null) {
    			UiApplication.getUiApplication().pushScreen(prevScreen);
    			
    			/*if (prevScreen instanceof FactsScreen) {
    				((FactsScreen) prevScreen).checkAddFavButtonState();
    			}*/
    		}
    		
    	} catch (Exception e) {
//    		Utils.showMessage("Error on OperPrevScreen - " + e.getMessage());
    	}
    }
    
    public CustomMainScreen getPrevScreen() {
    	return prevScreen;
    }
    
    protected void paintBackground(Graphics graphics) {
		super.paintBackground(graphics);
	}
	
	protected void paint(Graphics graphics) {
		super.paint(graphics);
	}
	
	public boolean navigationClick(int status, int time) {
		
		//trackpad/trackball click
		if ((status & KeypadListener.STATUS_FOUR_WAY) == KeypadListener.STATUS_FOUR_WAY) {
			//DataManager.playSound(Consts.SOUND_X_PATH);
		
			return super.navigationClick(status, time);
		}

		return true;
	}
	
	//Capturing key codes
	protected boolean keyDown(int keyCode, int time) {
		int key = Keypad.key(keyCode);

		if (key == Keypad.KEY_ENTER) {
		/*	
			//allow to start the game by pressing "Enter" on editField
			if (this instanceof SettingsScreen) {
				if (DataManager.isOnePlayerGame()) {
					if (((SettingsScreen)this).getFirstEditFieldState()) {
						return super.keyDown(keyCode, time);
					}
				} else {
					if (((SettingsScreen)this).getFirstEditFieldState() ||
							((SettingsScreen)this).getSecondEditFieldState()) {
						return super.keyDown(keyCode, time);
					}
				}
			}
			*/
			return true;
		}

		return super.keyDown(keyCode, time);
	}
	
}
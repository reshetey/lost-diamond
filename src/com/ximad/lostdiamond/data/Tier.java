package com.ximad.lostdiamond.data;

public interface Tier {
    int [][][] getTrucks();
    int [][] getCars();
    int [] getMin();
    String getDescription();
}

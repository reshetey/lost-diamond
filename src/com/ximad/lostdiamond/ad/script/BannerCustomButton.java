package com.ximad.lostdiamond.ad.script;

import com.ximad.lostdiamond.Utils;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class BannerCustomButton extends Field implements DrawStyle {

	private Bitmap currentImage = null;
	private Bitmap onImage = null;
	private Bitmap offImage = null;
	private Bitmap focusImage = null;
	private Bitmap checkImage = null;

	private int width;
	private int height;

	private boolean isFocusImageNeeded;
	private boolean isCheckImageNeeded;

	private boolean isFocused = false;

	private boolean checkState;

	public BannerCustomButton(Bitmap _offImage, Bitmap _onImage,
			Bitmap _focusImage, Bitmap _checkImage) {
		super(Field.EDITABLE);

		if (_offImage != null) {
			offImage = _offImage;

			width = offImage.getWidth();
			height = offImage.getHeight();

			currentImage = offImage;
		}

		if (_onImage != null) {
			onImage = _onImage;
		}

		if (offImage == null) {
			// Utils.displayAlert("�� ������� ����������� " + offImagePath);
		} else if (onImage == null) {
			// Utils.displayAlert("�� ������� ����������� " + onImagePath);
		}

		if (_focusImage == null) {
			isFocusImageNeeded = false;
		} else {
			focusImage = _focusImage;
			isFocusImageNeeded = true;

			width = focusImage.getWidth();
			height = focusImage.getHeight();
		}

		if (_checkImage == null) {
			isCheckImageNeeded = false;
		} else {
			checkImage = _checkImage;
			isCheckImageNeeded = true;
		}

		checkState = false;
	}

	public void repaint() {
		invalidate();
	}

	public int getPreferredHeight() {
		return height;
	}

	public int getPreferredWidth() {
		return width;
	}

	public boolean isFocusable() {
		return isEditable();
	}

	protected void onFocus(int direction) {
		currentImage = onImage;
		invalidate();
	}

	protected void onUnfocus() {
		currentImage = offImage;
		invalidate();
	}

	protected void layout(int _width, int _height) {
		setExtent(Math.min(_width, getPreferredWidth()), Math.min(_height,
				getPreferredHeight()));
	}

	// update the fieldchange
	protected void fieldChangeNotify(int context) {
		try {
			this.getChangeListener().fieldChanged(this, context);
		} catch (Exception e) {
//			Utils.showMessage("CustomButton error - " + e.getMessage());
		}
	}

	protected void drawFocus(Graphics graphics, boolean on) {
		isFocused = true;

		if (isFocusImageNeeded) {
			graphics.drawBitmap(0, 0, getWidth(), getHeight(), focusImage, 0,
					0);
		}
	}

	public boolean getFocusState() {
		return isFocused;
	}

	public void setOffImageByBitmap(Bitmap bitmap) {

		offImage = bitmap;
		
		 if (!getFocusState()) {
			 currentImage = offImage;
		 }

		invalidate();
	}

	public void setOnImageByBitmap(Bitmap bitmap) {

		onImage = bitmap;
		
		if (getFocusState()) {
			currentImage = onImage;
		}

		invalidate();
	}

	public int getOffImageWidth() {
		return offImage.getWidth();
	}

	public void setOnImage(Bitmap _onImage) {
		onImage = _onImage;

		if (getFocusState()) {
			currentImage = onImage;
		}

		invalidate();
	}
	
	public void setOffImage(Bitmap _offImage) {
		offImage = _offImage;

		if (!getFocusState()) {
			currentImage = offImage;
		}

		invalidate();
	}

	public void setFocusImage(Bitmap _focusImage) {
		if (_focusImage == null) {
			isFocusImageNeeded = false;
		} else {
			isFocusImageNeeded = true;
			focusImage = _focusImage;
		}

		invalidate();
	}

	public void setCheckState(boolean state) {
		checkState = state;
		invalidate();
	}

	public void setSize(int _width, int _height) {
		width = _width;
		height = _height;
	}

	public boolean getCheckState() {
		return checkState;
	}

	protected void paint(Graphics g) {
		isFocused = false;

		if (currentImage != null) {
			g.drawBitmap(0, 0, getWidth(), getHeight(), currentImage, 0, 0);
		}

		if (isCheckImageNeeded && checkState) {
			g.drawBitmap(0, 0, getWidth(), getHeight(), checkImage, 0, 0);
		}
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(1);
		return true;
	}
}
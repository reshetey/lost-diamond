package com.ximad.lostdiamond.ad.script;


import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.component.BitmapField;

public class BannerBitmapField extends BitmapField {
	
	Bitmap bitmap;
	
	public BannerBitmapField(Bitmap bitmap,
			long style) {
		super(bitmap, style);
		
		this.bitmap = bitmap;
	}
	
	public Bitmap getBitmap() {
		return bitmap;
	}
	
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
		super.setBitmap(bitmap);
	}
}
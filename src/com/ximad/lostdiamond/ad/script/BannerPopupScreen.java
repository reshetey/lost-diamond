package com.ximad.lostdiamond.ad.script;


import com.ximad.lostdiamond.Utils;
import com.ximad.lostdiamond.ad.AdHandler;
import com.ximad.lostdiamond.ad.Consts_Ad;
import com.ximad.lostdiamond.ad.CustomAd;
import com.ximad.lostdiamond.component.CustomButton;
import com.ximad.lostdiamond.component.CustomTextBox;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public final class BannerPopupScreen extends PopupScreen {

    BitmapField popupBg;

    CustomTextBox infoLabel;

    private String curAdText;
    private Bitmap curAdImage;
    private String curAdLink;

    private BannerBitmapField bannerField;
    private CustomTextBox bannerTextBox;
    private BannerCustomButton bannerButton;

    private CustomButton paidButton;

    private CustomButton skipButton;

    Manager layoutManager;
    private static final Bitmap AD_POPUP = Bitmap.getBitmapResource("ad_popup.png");
    private static final Bitmap BANNER_FOCUS = Bitmap.getBitmapResource("banner_focus.png");
    private static final Bitmap DEFAULT_BANNER = Bitmap.getBitmapResource("default_banner.jpg");
    private static final Bitmap BANNER_UNFOCUS = Bitmap.getBitmapResource("banner_unfocus.png");
    private static final Bitmap BANNER_PAID_OFF = Bitmap.getBitmapResource("paid_off.png");
    private static final String SKIP_OFF = "skip_off.png";
    private static final String SKIP_ON = "skip_on.png";
    private static final Bitmap SKIP_OFF_BITMAP = Bitmap.getBitmapResource(SKIP_OFF);

    public BannerPopupScreen() {
        super(new VerticalFieldManager());

        //������ ��� popup screen ����������
        this.setBackground(BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 0));

        curAdText = AdHandler.getBannerTextBox().getText();

        if (curAdText.equals("")) {
            curAdImage = ((BannerBitmapField)AdHandler.getBannerField()).getBitmap();
        } else {
            curAdImage = AdHandler.getDefaultBanner();
        }

        curAdLink = AdHandler.getClickLink();
        initLayout();
    }

    protected boolean navigationMovement(int dx, int dy, int i2, int i3) {
        if (dx != 0){
            return true;
        }

        return super.navigationMovement(dx, dy, i2, i3);
    }

    private void initLayout() {
//    	final int width = 320;//Display.getWidth();
        final int width = 480;//Display.getWidth();
//    	final int width = 360;//Display.getWidth();
//		final int height = 240;//Display.getHeight();
		final int height = 360;//Display.getHeight();
//        final int height = 320;//Display.getHeight();
//		final int height = 480;//Display.getHeight();

        popupBg = new BitmapField(AD_POPUP);

        infoLabel = new CustomTextBox(BANNER_FOCUS.getWidth(),
                BANNER_FOCUS.getHeight(),
//                Utils.getFont(Consts_Ad.FONT_FAMILY_NAME, Font.BOLD, 16), DrawStyle.HCENTER);
                Utils.getFont(Consts_Ad.FONT_FAMILY_NAME, Font.BOLD, 20), DrawStyle.HCENTER);
//                Utils.getFont(Consts_Ad.FONT_FAMILY_NAME, Font.BOLD, 24), DrawStyle.HCENTER);

        infoLabel.setText(Consts_Ad.AD_POPUP_TITLE);
        infoLabel.setFontColor(Color.GRAY);

        //
        // BANNER TEXT BOX
        //
        bannerTextBox = new CustomTextBox(Consts_Ad.BANNER_DEFAULT_WIDTH,
                Consts_Ad.BANNER_DEFAULT_HEIGHT,
                Utils.getFont(Consts_Ad.FONT_FAMILY_NAME, Font.PLAIN, 16), DrawStyle.HCENTER | DrawStyle.VCENTER);

        bannerTextBox.setFontColor(Color.LIGHTGOLDENRODYELLOW);
        bannerTextBox.setVerticalScroll(0);

        bannerTextBox.setText(curAdText);

        //
        // BANNER FIELD
        //

        bannerField = new BannerBitmapField(DEFAULT_BANNER,
                BitmapField.NON_FOCUSABLE) {
            public void paint(Graphics g) {
                if (!bannerTextBox.getText().equals("")) {
                    this.setBitmap(null);
                }

                super.paint(g);
            }
        };

        bannerField.setBitmap(curAdImage);

        //banner button

        bannerButton = new BannerCustomButton(BANNER_UNFOCUS,
                BANNER_UNFOCUS, BANNER_FOCUS, null);


//		//paid version button
        paidButton = new CustomButton("paid_off.png",
                "paid_on.png", null, null);
        //skip button
        skipButton = new CustomButton(SKIP_OFF,
                SKIP_ON, null, null);

        // Set layout manager
        layoutManager = new Manager(Manager.USE_ALL_WIDTH
                | Manager.USE_ALL_HEIGHT) {

            public void paint(Graphics g) {
                super.paint(g);
            }

            // layout manager components
            protected void sublayout(int w, int h) {
                layoutChild(infoLabel, w, h);
//				setPositionChild(infoLabel, (w - infoLabel.getWidth()) / 2, 20);
//				setPositionChild(infoLabel, (w - infoLabel.getWidth()) / 2, 50);
//                setPositionChild(infoLabel, (w - infoLabel.getWidth()) / 2, 30);
				setPositionChild(infoLabel, (w - infoLabel.getWidth()) / 2, 60);

				layoutChild(popupBg, w, h);
                setPositionChild(popupBg, 10, 10);

                layoutChild(bannerField, w, h);
//				setPositionChild(bannerField, (w - Consts_Ad.BANNER_DEFAULT_WIDTH) / 2, (h - Consts_Ad.BANNER_DEFAULT_HEIGHT) / 2 - 10);
//				setPositionChild(bannerField, (w - Consts_Ad.BANNER_DEFAULT_WIDTH) / 2, (h - Consts_Ad.BANNER_DEFAULT_HEIGHT) / 2);
//				setPositionChild(bannerField, (w - bannerField.getWidth()) / 2, (h - bannerField.getHeight()) / 2);
                setPositionChild(bannerField, (w - bannerField.getWidth()) / 2, (h - bannerField.getHeight()) / 2 - 20);

                //����������� ��������� ������ �������
                int txt_x = (Consts_Ad.BANNER_DEFAULT_WIDTH - bannerTextBox.getPreferredWidth()) / 2;
                if (txt_x < 0) {
                    txt_x = 0;
                }

                layoutChild(bannerTextBox, w, h);
                setPositionChild(bannerTextBox,	(w - Consts_Ad.BANNER_DEFAULT_WIDTH) / 2 + txt_x
//                        , (h - Consts_Ad.BANNER_DEFAULT_HEIGHT) / 2 - 10);
//						, (h - Consts_Ad.BANNER_DEFAULT_HEIGHT) / 2);
                        , (h - Consts_Ad.BANNER_DEFAULT_HEIGHT) / 2 - 20);

//				layoutChild(bannerButton, 228, 54);
				layoutChild(bannerButton, 340, 80);
//                layoutChild(bannerButton, 340, 71);
//				layoutChild(bannerButton, 260, 61);

//				setPositionChild(bannerButton, (w - 228) / 2, (h - BANNER_FOCUS.getHeight()) / 2 - 10);
//				setPositionChild(bannerButton, (w - 340) / 2, (h - BANNER_FOCUS.getHeight()) / 2);
                setPositionChild(bannerButton, (w - 340) / 2, (h - BANNER_FOCUS.getHeight()) / 2 - 20);
//                setPositionChild(bannerButton, (w - 260) / 2, (h - BANNER_FOCUS.getHeight()) / 2 - 20);

                layoutChild(paidButton, w, h);
//				setPositionChild(paidButton, (w - BANNER_PAID_OFF.getWidth()) / 2, 130);
//                setPositionChild(paidButton, (w - BANNER_PAID_OFF.getWidth()) / 2, 210);
                setPositionChild(paidButton, (w - BANNER_PAID_OFF.getWidth()) / 2, 180);
//				setPositionChild(paidButton, (w - BANNER_PAID_OFF.getWidth()) / 2, 280);
                layoutChild(skipButton, w, h);

//				setPositionChild(skipButton, (w - SKIP_OFF_BITMAP.getWidth())/2, 160);
//                setPositionChild(skipButton, (w - SKIP_OFF_BITMAP.getWidth())/2, 250);
                setPositionChild(skipButton, (w - SKIP_OFF_BITMAP.getWidth())/2, 220);
//				setPositionChild(skipButton, (w - SKIP_OFF_BITMAP.getWidth())/2, 330);
                setExtent(width, height);
            }
        };

        FieldChangeListener bannerListener = new FieldChangeListener() {
            public void fieldChanged(Field field, int context) {
                showAd();
            }
        };
//        bannerButton.setChangeListener(bannerListener);

        FieldChangeListener paidVersionListener = new FieldChangeListener() {
            public void fieldChanged(Field field, int context) {
                closeAdPopup();
                Browser.getDefaultSession().displayPage(Consts_Ad.PAID_VERSION_URL);
            }
        };
        paidButton.setChangeListener(paidVersionListener);

        FieldChangeListener skipListener = new FieldChangeListener() {
            public void fieldChanged(Field field, int context) {
                closeAdPopup();
            }
        };
        skipButton.setChangeListener(skipListener);


        layoutManager.add(popupBg);
        layoutManager.add(infoLabel);
        layoutManager.add(bannerButton);

        layoutManager.add(bannerField);
        layoutManager.add(bannerTextBox);

        layoutManager.add(paidButton);
        layoutManager.add(skipButton);

        add(layoutManager);
    }

    private void showAd() {
        closeAdPopup();
        CustomAd.clickPopupAction();

        AdHandler.adClick(curAdLink);
    }

    private void closeAdPopup() {
        UiApplication.getUiApplication().popScreen(this);
    }

    //������������ ���������� ����� � popup screen
    protected void applyTheme() {

    }

    protected boolean keyChar(char c, int status, int time) {
        if (c == Characters.ESCAPE) {
            close();
            return super.keyChar(c,status,time);
        }

        return false;
    }

    /*public void removeAdsFromScreen() {
		try {
			synchronized (Application.getEventLock()) {
				layoutManager.deleteAll();
			}
		} catch (Exception e) {
			//Utils.showMessage("removeAds Error: " + e.getMessage());
		}
	}*/

    public void close() {
        //removeAdsFromScreen();
    }

    protected boolean touchEvent(TouchEvent event) {
        int x = event.getX(1);
        int y = event.getY(1);

        int eventCode = event.getEvent();

        if(eventCode == TouchEvent.CLICK){
             if (x >= 50 && x <= 310 && y >=185 && y <= 255){
                 showAd();
            }
        }

        return super.touchEvent(event);
    }
}
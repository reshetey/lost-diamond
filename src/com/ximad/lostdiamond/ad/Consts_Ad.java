package com.ximad.lostdiamond.ad;

import net.rim.device.api.system.ApplicationDescriptor;

public final class Consts_Ad {
	
	public static final String USER_AGENT_STR = "User-Agent";
	public static final String NO_ADS_STRING = "<p>no ads</p>";
	public static final String FREQUENCY_PREFIX = "<!--";
	public static final String SRC_STRING = "src=\"";
	public static final String A_HREF_STRING = "a href=\"";
	
	public static final String DEFAULT_CLICK_LINK = "http://www.ximad.com";
	public static final String DEFAULT_IMAGE_LINK = "ximad banner image";
	public static final String ERROR_IMAGE_LINK = "error banner image";

	public static final String DEFAULT_BANNER_IMAGE = "default_banner.jpg";
	
	public static final String FONT_FAMILY_NAME = "BBAlpha Sans Condensed";

//	public static final int BANNER_DEFAULT_WIDTH = 200;
//    public static final int BANNER_DEFAULT_WIDTH = 300;
    public static final int BANNER_DEFAULT_WIDTH = 225;

//	public static final int BANNER_DEFAULT_HEIGHT = 50;
//    public static final int BANNER_DEFAULT_HEIGHT = 33;
//	public static final int BANNER_DEFAULT_HEIGHT = 44;
    public static final int BANNER_DEFAULT_HEIGHT = 38;

    //������� ���������� ������� (�� ���������) - 25 ���
	public static final int DEFAULT_FREQUENCY = 25000;
	
	//������, �� ������� �� ���������� ����� � ��������, � ������� ������� �� ������ � ��� - 5 ���.
	public static final int DEFAULT_POPUP_PERIOD = 5 * 60 * 1000;

	
	public final static String defaultAdTexts[] = {"Check out this useful site for FREE!", "FREE INFORMATION FOR YOU", "Click here to cheer up!",
		"DISCOVER IT FOR FREE!", "Visit this WONDERFUL page"};
	
	public final static String AD_POPUP_TITLE = "This app was brought free thanks for advertising support.";
	public final static String PAID_BUTTON_TEXT = "Get the Full version";
	
	public final static String PAID_VERSION_URL = "http://appworld.blackberry.com/webstore/content/31363";
	
	public final static int FONT_AD_TITLE_SIZE = 20;
	public final static int FONT_AD_TEXT_SIZE = 14;
	
	public static final int AD_POPUP_TITLE_Y = 18;
	
	public static final int BANNER_X = 60;
	public static final int BANNER_Y = 160;
	
	public static final int AD_POPUP_PAID_VERSION_Y = 145;
	public static final int AD_POPUP_SKIP_Y = 173;
	//Default AD config values
	public static final String DEFAULT_SHORT_NAME = "lostdiamond_sdk";
	public static final boolean DEFAULT_TRANSPERA_ENB = true;
	public static final String DEFAULT_TRANSPERA_CCH = "";
	public static final int DEFAULT_TRANSPERA_PID = 2492;
	public static final String DEFAULT_VERSION = ApplicationDescriptor.currentApplicationDescriptor().getVersion();
	                              
	//Ad requests links
	public final static String SCRIPT_PARAMS_URL = "http://ad2.ximad.com/ad.php?platform=blackberry&name=" + DEFAULT_SHORT_NAME
	+ "&version=" + DEFAULT_VERSION
	+ "&width=300&height=50";
	public final static String SCRIPT_AD_URL = "http://ad1.ximad.com/ad.php?platform=blackberry&version=" + DEFAULT_VERSION
		+"&width=300&height=50&name=" + DEFAULT_SHORT_NAME;
	public static final String TRANSPERA_AD_URL = "http://blackberry-sdk.transpera.com/tap/ad/Ad?adtype=video&mode=7";

	//Identifier for example log output
	public static final long TRANSPERA_GUID = 0x1a7494e091ae464eL;
	
	// User defined request code used to identify when an ad finishes playing (must be >= 0)
	public static final int TRANSPERA_REQUEST_CODE = 0xBEEF;
	
	//AD NETWORKS	
	public static final String LOCALYTICS_APP_KEY = "c5995659cd0eb51f4799438-975810aa-3372-11e0-c2ad-007af5bd88a0";
}

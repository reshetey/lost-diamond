package com.ximad.lostdiamond;

import com.ximad.lostdiamond.menu.BlocksScreen;
import com.ximad.lostdiamond.ad.CustomAd;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class LevelFinishedScreen extends PopupScreen {
    BitmapField levelFinished = new BitmapField(Bitmap.getBitmapResource("popup_level_finished.png"));

    public LevelFinishedScreen() {
        super(new HorizontalFieldManager());
        this.setBackground(BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 0));

        Manager manager = new Manager(Manager.USE_ALL_WIDTH
                | Manager.USE_ALL_HEIGHT) {
            protected void sublayout(int i, int i1) {
                layoutChild(levelFinished, 480, 360);

                setPositionChild(levelFinished, 120, 80);

                setExtent(i, i1);
            }
        };

        manager.add(levelFinished);

        add(manager);

        
    }

    protected void applyTheme() {
    }

}
package com.ximad.lostdiamond;

import com.ximad.lostdiamond.ad.Consts_Ad;
import net.rim.device.api.math.Fixed32;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.*;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

import java.util.Random;

public final class Utils {

	private static Random random = new Random();

	public static int getRandomNumber(int bound) {
		if(bound <= 0)
			return 0;
		else
			return random.nextInt(bound);
	}

	public static Font getFont(String familyName, int style, int fontSize)
	{
		try
		{
			return FontFamily.forName(familyName).getFont(style, fontSize);
		}
		catch (Exception e)
		{
			return Font.getDefault().getFontFamily().getFont(style, fontSize);
		}
	}

	/**
	 * Presents a dialog to the user with a given message
	 *
	 * @param message
	 *            The text to display
	 */
	public static void showMessage(final String message) {
		UiApplication.getUiApplication().invokeAndWait(new Runnable() {
			public void run() {
				Dialog.alert(message);
			}
		});
	}

	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getRandomDefaultAdText() {
		return Consts_Ad.defaultAdTexts[random
				.nextInt(Consts_Ad.defaultAdTexts.length)];
	}

	public static Bitmap resizeBanner(byte[] data, int newWidth, int newHeight) {

		//Utils.showMessage("w = " + bitmap.getWidth() + " h = " + bitmap.getHeight());

		EncodedImage image = EncodedImage.createEncodedImage(data, 0, data.length);

		int scaleFactorX = Fixed32.div(Fixed32.toFP(image.getWidth()), Fixed32
				.toFP(newWidth));
		int scaleFactorY = Fixed32.div(Fixed32.toFP(image.getHeight()), Fixed32
				.toFP(newHeight));

		return image.scaleImage32(scaleFactorX, scaleFactorY).getBitmap();
	}


	public static String getConnectionSuffix() {
		if (DeviceInfo.isSimulator()) {
			return ";deviceside=true";
		} else if ((WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED)
				&& RadioInfo.areWAFsSupported(RadioInfo.WAF_WLAN)) {
			return ";interface=wifi";
		} else {
			String uid = null;
			ServiceBook sb = ServiceBook.getSB();
			ServiceRecord[] records = sb.findRecordsByCid("WPTCP");
			for (int i = 0; i < records.length; i++) {
				if (records[i].isValid() && !records[i].isDisabled()) {
					if (records[i].getUid() != null
							&& records[i].getUid().length() != 0) {
						if ((records[i].getCid().toLowerCase().indexOf("wptcp") != -1)
								&& (records[i].getUid().toLowerCase().indexOf(
										"wifi") == -1)
								&& (records[i].getUid().toLowerCase().indexOf(
										"mms") == -1)) {
							uid = records[i].getUid();
							break;
						}
					}
				}
			}
			if (uid != null) {
				// WAP2 Connection
				return ";ConnectionUID=" + uid;
			} else {
				return ";deviceside=true";
			}
		}
	};

	public static String getUserAgent() {
		String version = "";
		ApplicationDescriptor[] ad = ApplicationManager.getApplicationManager()
				.getVisibleApplications();
		for (int i = 0; i < ad.length; i++) {
			if (ad[i].getModuleName().trim().equalsIgnoreCase(
					"net_rim_bb_ribbon_app")) {
				version = ad[i].getVersion();
				break;
			}
		}

		return "Blackberry" + DeviceInfo.getDeviceName() + "/" + version
				+ " Profile/" + System.getProperty("microedition.profiles")
				+ " Configuration/"
				+ System.getProperty("microedition.configuration")
				+ " VendorID/" + Branding.getVendorId();
	}

	public static String replaceString(String aSearch, String aFind,
			String aReplace) {
		String result = aSearch;
		if (result != null && result.length() > 0) {
			int a = 0;
			int b = 0;
			while (true) {
				a = result.indexOf(aFind, b);
				if (a != -1) {
					result = result.substring(0, a) + aReplace
							+ result.substring(a + aFind.length());
					b = a + aReplace.length();
				} else
					break;
			}
		}
		return result;
	}
}

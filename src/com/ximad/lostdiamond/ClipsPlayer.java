package com.ximad.lostdiamond;
// ClipsPlayer.java

import net.rim.device.api.media.control.AudioPathControl;

import javax.microedition.media.Control;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import java.util.Enumeration;
import java.util.Hashtable;


public class ClipsPlayer implements PlayerListener
{
    private final static String SOUND_DIR = "/";

    private Hashtable clipsMap;
    // the key is the filename string, the value is a Player object
    private boolean isDeviceAvailable = true;



    public ClipsPlayer()
    {  clipsMap = new Hashtable();  }



    public boolean load(final String fnm)
    // create a Player object for fnm and store it
    {
        if (clipsMap.containsKey(fnm)) {
//      System.out.println(fnm + "already stored");
            return true;
        }
        Player clip = loadClip(fnm);
        if (clip != null) {
            clipsMap.put(fnm, clip);
        }
        return false;
    }  // end of load()


    private Player loadClip(final String fnm)
    {
        String contentType = extractContentType(fnm);

        Player clip = null;
        try {
            clip = Manager.createPlayer(
                    getClass().getResourceAsStream(SOUND_DIR  + fnm),
                    contentType);
            clip.addPlayerListener(this);
            clip.realize();
            clip.prefetch();     // prepare audio resources needed by player
            useSpeaker(clip);
        }
        catch (Exception e) {
        }
        return clip;
    }  // end of loadClip()


    private void useSpeaker(Player clip)
    // make sure the player uses the device's speaker
    {
        try {
            AudioPathControl apc = null;
            Control[] ctrls = clip.getControls();
            for(int i = ctrls.length-1; i >= 0; i--)
                if(ctrls[i] instanceof AudioPathControl) {
                    apc = (AudioPathControl) ctrls[i];
                    break;
                }
            if(apc != null)
                apc.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSFREE);  // speaker
        }
        catch (Exception e) {
        }
    }  // end of useSpeaker()



    private String extractContentType(String fnm)
        /* choose the content type used by a player based on the clip's
      file extension */
    {
        int lastDot = fnm.lastIndexOf('.');
        if (lastDot == -1)
            return "audio/x-wav";   // default content type
        String extStr = fnm.substring(lastDot+1).toLowerCase();

        if (extStr.endsWith("au"))
            return "audio/basic";
        else if (extStr.endsWith("mp3"))
            return "audio/mpeg";
        else if (extStr.endsWith("mid"))
            return "audio/midi";
        else if (extStr.endsWith("jts"))   // tone sequences
            return "audio/x-tone-seq";

        return "audio/x-wav";   // default content type
    }  // end of extractContentType()


    public boolean play(String fnm)
    // play a clip once
    {  return play(fnm, false);  }



    private boolean play(final String fnm, boolean isRepeating)
    // play a clip once or multiple times
    {
        if (!isDeviceAvailable) {
            return false;
        }

        Player clip = (Player) clipsMap.get(fnm);
        if (clip == null) {
            return false;
        }

        if (clip.getState() == Player.STARTED){
            return true;
        }

        try {
            if (isRepeating)
                clip.setLoopCount(-1);  // play indefinitely
            clip.start();
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }  // end of play()


    public boolean loadPlay(String fnm)
    // load and play a clip once
    {
        boolean isLoaded = load(fnm);
        if (!isLoaded)
            return false;
        else
            return play(fnm);
    }  // end of loadPlay()




    public boolean isPlaying(final String fnm)
    // is the specified clip currently playing
    {
        if (!isDeviceAvailable) {

            return false;
        }

        Player clip = (Player) clipsMap.get(fnm);
        if (clip == null) {
            return false;
        }

        return (clip.getState() == Player.STARTED);
    }  // end of isPlaying()



    public boolean loop(String fnm)
    // keep repeating the clip
    {  return play(fnm, true);  }


    public boolean loadLoop(String fnm)
    // load and play a clip repeatedly
    {
        boolean isLoaded = load(fnm);
        if (!isLoaded)
            return false;
        else
            return loop(fnm);
    }  // end of loadPlay()



    public boolean stop(String fnm)
    // stop playing a clip
    {
        Player clip = (Player) clipsMap.get(fnm);
        if (clip == null) {
//            System.out.println("No loaded clip for " + fnm);
            return false;
        }
        try {
            clip.stop();
            return true;
        }
        catch(Exception e){
//            System.out.println("Could not stop " + fnm);
            return false;
        }
    }  // end of stop()



    public void close()
    // close all the players
    {
        Enumeration keys = clipsMap.keys();
        while (keys.hasMoreElements())
            close( (String) keys.nextElement() );
    }


    public void close(String fnm)
    // close a player
    {
        Player clip = (Player) clipsMap.get(fnm);
        if (clip != null) {
            try {
                clip.stop();
                clip.deallocate();
                clip.close();
                clip = null;
            }
            catch(Exception e){}
        }
    }  // end of closes()



    public void playerUpdate(Player clip, String event, Object eventData)
    {
        try {
            if (event.equals(PlayerListener.DEVICE_UNAVAILABLE))      // incoming phone call
                isDeviceAvailable = false;
            else if (event.equals(PlayerListener.DEVICE_AVAILABLE))   // finished phone call
                isDeviceAvailable = true;

//            System.out.println("playerUpdate() event: " + event);
        }
        catch (Exception e)
        {  System.out.println(e); }
    }  // end of playerUpdate()

}  // end of ClipsPlayer class

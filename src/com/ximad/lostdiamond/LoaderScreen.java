package com.ximad.lostdiamond;

import com.ximad.lostdiamond.menu.BlocksScreen;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LoaderScreen extends MainScreen {
    BitmapField splash = new BitmapField(Bitmap.getBitmapResource("splash.jpg"));

    int imageIndex = 0;
    BitmapField bar = new BitmapField();
    Bitmap bars[] = new Bitmap[9];

    {
        bars[0] = Bitmap.getBitmapResource("bar_0.png");
        bars[1] = Bitmap.getBitmapResource("bar_1.png");
        bars[2] = Bitmap.getBitmapResource("bar_2.png");
        bars[3] = Bitmap.getBitmapResource("bar_3.png");
        bars[4] = Bitmap.getBitmapResource("bar_4.png");
        bars[5] = Bitmap.getBitmapResource("bar_5.png");
        bars[6] = Bitmap.getBitmapResource("bar_6.png");
        bars[7] = Bitmap.getBitmapResource("bar_7.png");
        bars[8] = Bitmap.getBitmapResource("bar_8.png");
    }

    class S extends VerticalFieldManager{
        protected void sublayout(int i, int i1) {
            layoutChild(splash, 480, 360);

            setPositionChild(splash, 0, 0);

            bar.setBitmap(bars[imageIndex]);
            layoutChild(bar, 314, 34);
            setPositionChild(bar, 100, 300);

            setExtent(480, 360);
        }

        public void refresh(){
            updateLayout();
        }
    }

    S manager = new S();

    public LoaderScreen() {
        super();

        manager.add(splash);

        manager.add(bar);

        add(manager);



        new Thread(){
            public void run() {
                for (int i = 0; i < bars.length - 1; i++) {

                    UiApplication.getUiApplication().invokeLater(new Runnable() {
                        public void run() {
                            manager.refresh();
                            imageIndex++;
                        }
                    });
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                    }
                }

                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
                        UiApplication.getUiApplication().popScreen(LoaderScreen.this);

                        UiApplication.getUiApplication().pushScreen(new BlocksScreen());
                    }
                });
            }
        }.start();

    }

}
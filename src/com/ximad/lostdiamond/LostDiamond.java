package com.ximad.lostdiamond;

import com.ximad.lostdiamond.ad.AdHandler;
import com.ximad.lostdiamond.ad.Consts_Ad;
import com.ximad.lostdiamond.ad.localytics.LocalyticsSession;
import com.ximad.lostdiamond.menu.BlocksScreen;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;

/**
 * This class extends the UiApplication class, providing a
 * graphical user interface.
 */
public class LostDiamond extends UiApplication
{
    private LocalyticsSession localyticsSession;

    /**
     * Entry point for application
     * @param args Command line arguments (not used)
     */
    public static void main(String[] args)
    {
        // Create a new instance of the application and make the currently
        // running thread the application's event dispatch thread.
        LostDiamond theApp = new LostDiamond();
        theApp.enterEventDispatcher();

    }


    /**
     * Creates a new HelloWorldDemo object
     */
    public LostDiamond()
    {

        int directions = Display.DIRECTION_PORTRAIT;
        Ui.getUiEngineInstance().setAcceptableDirections(directions);
        localyticsSession = new LocalyticsSession(Consts_Ad.LOCALYTICS_APP_KEY);
		localyticsSession.open();
		localyticsSession.upload();


		AdHandler adHandler = new AdHandler();
		adHandler.start();
		AdHandler.setAdTextColor(Color.BLACK);

        // Push a screen onto the UI stack for rendering.
//        pushScreen(new BlocksScreen());
        pushScreen(new LoaderScreen());
    }

    //�������������� ���������� �� ����������
    public void activate() {

        //

    }

    //������������ ����������
    public void deactivate() {
//        SoundHandler.stopSounds(SoundResources.getAllSoundPlayers());
    }

    // LOCALYTICS

    public LocalyticsSession getLocalyticsSession() {
        return this.localyticsSession;
    }

    public void exit() {
        this.localyticsSession.close();
        System.exit(0);
    }

}